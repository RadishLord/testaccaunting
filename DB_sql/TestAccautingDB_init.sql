﻿-- Table: distractor

-- DROP TABLE distractor;

CREATE TABLE distractor
(
  id_distractor serial NOT NULL,
  number_distractor integer NOT NULL,
  text_distractor character varying(500) NOT NULL,
  is_right boolean NOT NULL,
  id_quest integer,
  CONSTRAINT id_distractort_pkey PRIMARY KEY (id_distractor),
  CONSTRAINT id_quest_fkey1 FOREIGN KEY (id_quest)
      REFERENCES question (id_quest) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE distractor
  OWNER TO postgres;

-- Trigger: distr_count_decrement_trigger on distractor

-- DROP TRIGGER distr_count_decrement_trigger ON distractor;

CREATE TRIGGER distr_count_decrement_trigger
  BEFORE DELETE
  ON distractor
  FOR EACH ROW
  EXECUTE PROCEDURE distr_count_decrement();

-- Trigger: distr_count_increment_trigger on distractor

-- DROP TRIGGER distr_count_increment_trigger ON distractor;

CREATE TRIGGER distr_count_increment_trigger
  BEFORE INSERT
  ON distractor
  FOR EACH ROW
  EXECUTE PROCEDURE distr_count_increment();

-- Table: question

-- DROP TABLE question;

CREATE TABLE question
(
  id_quest serial NOT NULL,
  id_test integer NOT NULL,
  text_quest character varying(500) NOT NULL,
  number_quest integer NOT NULL,
  cost_quest real,
  count_distractor integer,
  CONSTRAINT id_quest_pkey PRIMARY KEY (id_quest),
  CONSTRAINT id_test_fkey1 FOREIGN KEY (id_test)
      REFERENCES test (id_test) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE question
  OWNER TO postgres;

-- Table: study_group

-- DROP TABLE study_group;

CREATE TABLE study_group
(
  id_group serial NOT NULL,
  name_group character varying(20) NOT NULL,
  cathedra character varying(30) NOT NULL,
  recruitment_year date,
  cipher character varying(10),
  CONSTRAINT id_group_pkey PRIMARY KEY (id_group),
  CONSTRAINT name_group_unique UNIQUE (name_group)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE study_group
  OWNER TO postgres;

-- Table: teacher

-- DROP TABLE teacher;

CREATE TABLE teacher
(
  id_teacher serial NOT NULL,
  fio character varying(50) NOT NULL,
  password character varying(30) NOT NULL,
  login character varying(30) NOT NULL,
  CONSTRAINT id_teacher_pkey PRIMARY KEY (id_teacher)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE teacher
  OWNER TO postgres;

-- Table: test

-- DROP TABLE test;

CREATE TABLE test
(
  id_test serial NOT NULL,
  id_teacher integer NOT NULL,
  name_test character varying(50) NOT NULL,
  question_count integer,
  "time" interval,
  CONSTRAINT id_test_pkey PRIMARY KEY (id_test),
  CONSTRAINT id_teacher_fkey FOREIGN KEY (id_teacher)
      REFERENCES teacher (id_teacher) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE test
  OWNER TO postgres;

-- Table: test_result

-- DROP TABLE test_result;

CREATE TABLE test_result
(
  id_test_result integer NOT NULL DEFAULT nextval('test_resoults_id_test_resoults_seq'::regclass),
  id_user integer NOT NULL,
  id_test integer NOT NULL,
  total_quest integer NOT NULL,
  wrong_quest integer NOT NULL,
  right_quest integer NOT NULL,
  missed_quest integer NOT NULL,
  time_passed interval,
  mark integer NOT NULL,
  percent integer NOT NULL,
  attempt_number integer NOT NULL,
  "time" timestamp with time zone,
  CONSTRAINT id_test_resoults_pkey PRIMARY KEY (id_test_result),
  CONSTRAINT id_student_fkey FOREIGN KEY (id_user)
      REFERENCES users (id_user) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT id_test_fkey FOREIGN KEY (id_test)
      REFERENCES test (id_test) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE test_result
  OWNER TO postgres;

-- Table: users

-- DROP TABLE users;

CREATE TABLE users
(
  id_user serial NOT NULL,
  id_group integer NOT NULL,
  fio character varying(50) NOT NULL,
  login character varying(50) NOT NULL,
  passw character varying(20),
  CONSTRAINT id_user_pkey PRIMARY KEY (id_user),
  CONSTRAINT id_group_fkey FOREIGN KEY (id_group)
      REFERENCES study_group (id_group) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE users
  OWNER TO postgres;

-- Function: distr_count_decrement()

-- DROP FUNCTION distr_count_decrement();

CREATE OR REPLACE FUNCTION distr_count_decrement()
  RETURNS trigger AS
$BODY$
BEGIN

	UPDATE question   
	SET count_distractor = count_distractor - 1 WHERE id_quest = old.id_quest;

        
        RETURN old;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION distr_count_decrement()
  OWNER TO postgres;

-- Function: distr_count_increment()

-- DROP FUNCTION distr_count_increment();

CREATE OR REPLACE FUNCTION distr_count_increment()
  RETURNS trigger AS
$BODY$
BEGIN

	UPDATE question   
	SET count_distractor = count_distractor + 1 WHERE id_quest = new.id_quest;

        
        RETURN NEW;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION distr_count_increment()
  OWNER TO postgres;

-- Function: login_length_check_trigger_func()

-- DROP FUNCTION login_length_check_trigger_func();

CREATE OR REPLACE FUNCTION login_length_check_trigger_func()
  RETURNS trigger AS
$BODY$
DECLARE
BEGIN

	IF (length(new.login) < 5) THEN
		RAISE NOTICE 'Слишком короткий логин';
		return old;
	END IF;
	
        RETURN new;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION login_length_check_trigger_func()
  OWNER TO postgres;

-- Function: login_uniq_check_trigger_func()

-- DROP FUNCTION login_uniq_check_trigger_func();

CREATE OR REPLACE FUNCTION login_uniq_check_trigger_func()
  RETURNS trigger AS
$BODY$
DECLARE
BEGIN

	IF (EXISTS (SELECT * FROM users WHERE login = new.login)) THEN
		RAISE NOTICE 'Логин существует';
		return old;
	END IF;
	
        RETURN new;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION login_uniq_check_trigger_func()
  OWNER TO postgres;

-- Function: pass_length_check_trigger_func()

-- DROP FUNCTION pass_length_check_trigger_func();

CREATE OR REPLACE FUNCTION pass_length_check_trigger_func()
  RETURNS trigger AS
$BODY$
DECLARE
BEGIN

	IF (length(new.passw) < 5) THEN
		RAISE NOTICE 'Ненадежный пароль';
		return old;
	END IF;
	
        RETURN new;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION pass_length_check_trigger_func()
  OWNER TO postgres;

-- Function: add_distr(integer, character varying, boolean, integer)

-- DROP FUNCTION add_distr(integer, character varying, boolean, integer);

CREATE OR REPLACE FUNCTION add_distr(i_number integer, i_text character varying, i_is_right boolean, i_id_quest integer)
  RETURNS void AS
$BODY$
BEGIN
	
	INSERT INTO distractor (number_distractor, text_distractor, is_right, id_quest) VALUES
	(i_number, i_text, i_is_right, i_id_quest);

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION add_distr(integer, character varying, boolean, integer)
  OWNER TO postgres;

-- Function: add_distr(integer, character varying, boolean, integer)

-- DROP FUNCTION add_distr(integer, character varying, boolean, integer);

CREATE OR REPLACE FUNCTION add_distr(i_number integer, i_text character varying, i_is_right boolean, i_id_quest integer)
  RETURNS void AS
$BODY$
BEGIN
	
	INSERT INTO distractor (number_distractor, text_distractor, is_right, id_quest) VALUES
	(i_number, i_text, i_is_right, i_id_quest);

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION add_distr(integer, character varying, boolean, integer)
  OWNER TO postgres;
