﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestAccaunting.DataBase;

namespace TestAccaunting
{
    class Authentication
    {
        private bool _loginIsCorrect;
        public bool LoginIsCorrect
        {
            get
            {
                return _loginIsCorrect;
            }
        }
        private bool _passwordIsCorrect;
        public bool PasswordIsCorrect
        {
            get
            {
                return _passwordIsCorrect;
            }
        }

        public bool IsSuccess(User user, DBManager dBManager)
        {
            dBManager.LoginAndPassCheck(user, out _loginIsCorrect, out _passwordIsCorrect);

            return _loginIsCorrect && _passwordIsCorrect;

        }

        public User GetUserData(User user, DBManager dBManager)
        {
            return dBManager.GetTeacherData(user);
        }
    }
}
