﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestAccaunting.DataBase;

namespace TestAccaunting.ProgramEntrance
{
    class Registration
    {
        private bool _loginIsCorrect;
        public bool LoginIsCorrect
        {
            get
            {
                return _loginIsCorrect;
            }
        }
        private bool _passwordIsCorrect;
        public bool PasswordIsCorrect
        {
            get
            {
                return _passwordIsCorrect;
            }
        }

        public bool IsSuccess(User user, DBManager dBManager)
        {
            dBManager.RegisterNewTeacher(user, out _loginIsCorrect);

            return _loginIsCorrect;

        }
    }
}
