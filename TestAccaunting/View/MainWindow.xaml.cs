﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TestAccaunting.Data;
using TestAccaunting.View;
using TestAccaunting.ViewModel;

namespace TestAccaunting
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //--declare--//
        User user;
        GroupCollection groupCollection;
        GroupViewModel groupViewModel;

        StudentCollection studentCollection;
        StudentViewModel studentViewModel;

        TestsCollection testsCollection;
        TestViewModel testViewModel;

        TestResultCollection testResultCollection;
        TestResultViewModel testResultViewModel;
        //---------//

        public MainWindow(User _user)
        {
            InitializeComponent();
            user = _user;
            Loaded += MainWindow_Loaded;
            groupCollection = new GroupCollection(App.DBManager);
            groupViewModel = new GroupViewModel(groupCollection);

            studentCollection = new StudentCollection(App.DBManager);
            studentViewModel = new StudentViewModel(studentCollection);

            testsCollection = new TestsCollection(App.DBManager, user);

            testResultCollection = new TestResultCollection(App.DBManager);
            testResultViewModel = new TestResultViewModel(testResultCollection);

        }

        //TODO delete this
        public MainWindow()
        {
            InitializeComponent();
            user = new User("TestUser", "1234", "Иванов Иван Иванович") {Id = 8};
            Loaded += MainWindow_Loaded;
            groupCollection = new GroupCollection(App.DBManager);
            groupViewModel = new GroupViewModel(groupCollection);

            studentCollection = new StudentCollection(App.DBManager);
            studentViewModel = new StudentViewModel(studentCollection);

            testsCollection = new TestsCollection(App.DBManager, user);

            testResultCollection = new TestResultCollection(App.DBManager);
            testResultViewModel = new TestResultViewModel(testResultCollection);

        }

        //--Загрузка формы/привязка контекста
        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            MainGrid.DataContext = user;

            TabGroups.DataContext = groupViewModel;
            TabStudents.DataContext = studentViewModel;
            comboBoxTests.ItemsSource = testsCollection.GetStudentCollectionToString();
            ComboBoxTestResultGroup.ItemsSource = groupCollection.GetGroupCollectionToString();
            ComboBoxTestResultTest.ItemsSource = testsCollection.GetStudentCollectionToString();
            DataGridTestResult.DataContext = testResultViewModel;

        }
        //--loguot
        private void Logout_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Вы действительно хотите выйти?\nВсе несохраненные изменения будут утеряны!", "", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {

                LoginWindow loginWindow = new LoginWindow();
                loginWindow.Show();
                this.Close();
            }
        }

        #region Смена рабочих панелей
        private void ButtonGroup_Click(object sender, RoutedEventArgs e)
        {
            MainTabControl.SelectedIndex = 3;
            MainTabControl.SelectedItem = TabGroups;
            LabelPanelName.Content = "Группы";
        }

        private void ButtonTests_Click(object sender, RoutedEventArgs e)
        {
            MainTabControl.SelectedIndex = 2;
            MainTabControl.SelectedItem = TabTests;
            LabelPanelName.Content = "Тесты";
        }

        private void ButtonStudents_Click(object sender, RoutedEventArgs e)
        {
            MainTabControl.SelectedIndex = 1;
            MainTabControl.SelectedItem = TabStudents;
            LabelPanelName.Content = "Студенты";
        }

        private void ButtonResult_Click(object sender, RoutedEventArgs e)
        {
            MainTabControl.SelectedIndex = 4;
            MainTabControl.SelectedItem = TabResult;
            LabelPanelName.Content = "Результаты тестов";
        }
        #endregion

        #region DataGridGroup_Команды
        //--DataGridGroup_ Удалить
        private void ButtonGroupDelete_Click(object sender, RoutedEventArgs e)
        {

            Group delGroup = (Group)DataGridGroup.SelectedItem;
            if (delGroup == null)
            {
                MessageBox.Show("Строка не выбрана");
            }
            if (MessageBox.Show($"Вы действительно хотите удалить эту группу?\n- {delGroup.Name} ({delGroup.Cathedra})", "", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                if (!groupCollection.CheckGroupReferences(delGroup))
                {
                    groupCollection.DeleteGroup(delGroup);
                }
                else
                {
                    MessageBox.Show("Удаление записи невозможно, т.к. имеются ссылки на другие таблицы");
                }
 
            }
        }
        //--DataGridGroup_ Добавить
        private void ButtonGroupAdd_Click_1(object sender, RoutedEventArgs e)
        {
            
            AddNewGroupWindow addNewGroupWindow = new AddNewGroupWindow();

            if (addNewGroupWindow.ShowDialog() == true)
            {
                Group newGroup = addNewGroupWindow.GetNewGroup();
                groupCollection.GroupList.Add(newGroup);
                App.DBManager.AddGroup(newGroup);
                groupCollection.Reset(App.DBManager);
            }

        }
        //--DataGridGroup_ Обновить
        private void ButtonGroupRefresh_Click(object sender, RoutedEventArgs e)
        {
            groupCollection.Reset(App.DBManager);
        }
        //--DataGridGroup_ Сохранить
        private void ButtonGroupSave_Click(object sender, RoutedEventArgs e)
        {
            groupCollection.AllSave();
            groupCollection.Reset(App.DBManager);
        }
        #endregion

        #region DataGridGroup_Обработчики
        //--DataGridGroup_ Обработчик добавления
        private void DataGridGroup_AddingNewItem(object sender, AddingNewItemEventArgs e)
        {
            
        }
        //--DataGridGroup_ Обработчик модификации
        private void DataGridGroup_BeginningEdit(object sender, DataGridBeginningEditEventArgs e)
        {
            //MessageBox.Show("mod");
            if (e.Column.DisplayIndex == 2)
            {
                e.Cancel = true;
            }
            else
            {
                groupCollection.isChange(DataGridGroup.SelectedItem as Group);
            }
        }
        //--DataGridGroup_ Обработчик хоткеев
        private void DataGridGroup_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete)
            {
                Group delGroup = (Group)DataGridGroup.SelectedItem;
                if (MessageBox.Show($"Вы действительно хотите удалить эту группу?\n- {delGroup.Name} ({delGroup.Cathedra})", "", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    groupCollection.DeleteGroup(delGroup);
                }
            }
        }

        #endregion

        #region Students panel_Обработчики
        //--DataGridStudents_ Обработчик добавления
        private void DataGridStudents_AddingNewItem(object sender, AddingNewItemEventArgs e)
        {

        }

        private void DataGridStudents_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete)
            {
                if (MessageBox.Show("Вы действительно хотите удалить?", "", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    Student delStudent = (Student)DataGridStudents.SelectedItem;
                    studentCollection.DeleteStudent(delStudent);
                }
            }
        }

        private void DataGridStudents_BeginningEdit(object sender, DataGridBeginningEditEventArgs e)
        {
            studentCollection.isChange(DataGridStudents.SelectedItem as Student);
        }
        //--Событие при загрузки панели студентов
        private void GridStudents_Loaded(object sender, RoutedEventArgs e)
        {
            ComboBoxStudentCourse.ItemsSource = new string[] { "1", "2", "3", "4", "5" };
            ComboBoxRefresh();
        }
        //--обновление комбобоксов и коллекции студентов в соответсвии с параметрами
        private void ComboBoxRefresh()
        {
            //--инициализация списка кафедр
            ComboBoxStudentsCathedra.ItemsSource = App.DBManager.GetAllCathedra();
            //--инициализация списка групп
            string year = "(все)";
            if (ComboBoxStudentCourse.SelectedItem != null)
                year = ComboBoxStudentCourse.SelectedItem.ToString();
            ComboBoxStudentGroup.ItemsSource = groupCollection.GetGroupCollectionToString(ComboBoxStudentsCathedra.Text, year);

            string cathedra = String.Empty;
            string Group = String.Empty;
            string Course = String.Empty;

            if (ComboBoxStudentsCathedra.SelectedItem != null)
                cathedra = ComboBoxStudentsCathedra.SelectedItem.ToString();
            if (ComboBoxStudentGroup.SelectedItem != null)
                Group = ComboBoxStudentGroup.SelectedItem.ToString();
            if (ComboBoxStudentCourse.SelectedItem != null)
                Course = ComboBoxStudentCourse.SelectedItem.ToString();


            studentCollection.GetWithParam(cathedra, Group, Course);
        }
        //--событие после каждой смены выбранного элемента
        private void ComboBoxRefresh(object sender, SelectionChangedEventArgs e)
        {
            ComboBoxRefresh();
        }

        #endregion

        #region Students panel_Команды
        //--DataGridStudents_ Удалить
        private void ButtonStudentDelete_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Вы действительно хотите удалить?", "", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {

                Student DeleteStudent = (Student)DataGridStudents.SelectedItem;
                if (!studentCollection.CheckReferences(DeleteStudent))
                {
                    studentCollection.DeleteStudent(DeleteStudent);
                }
                else
                {
                    MessageBox.Show("Удаление студента невозможно т.к. имеются ссылки на другие таблицы.");
                }

            }
        }
        //--DataGridStudents_ Добавить
        private void ButtonStudentAdd_Click(object sender, RoutedEventArgs e)
        {
            
            AddNewStudentWindow addNewStudentWindow = new AddNewStudentWindow(groupCollection);

            if (addNewStudentWindow.ShowDialog() == true)
            {
                Student newStudent = addNewStudentWindow.GetStudent();
                newStudent.IdGroup = groupCollection.GetId(newStudent.Group);
                studentCollection.StudentList.Add(newStudent);
                App.DBManager.AddStudent(newStudent);
                studentCollection.Reset(App.DBManager);
                ComboBoxRefresh();
            }
        }
        //--DataGridStudents_ Обновить
        private void ButtonStudentRefresh_Click(object sender, RoutedEventArgs e)
        {
            studentCollection.Reset(App.DBManager);
            ComboBoxRefresh();
        }
        //--DataGridStudents_ Сохранить
        private void ButtonStudentSave_Click(object sender, RoutedEventArgs e)
        {
            studentCollection.AllSave();
            studentCollection.Reset(App.DBManager);
            ComboBoxRefresh();
        }






        #endregion

        #region Tests
        //Удалить TEST
        private void buttonTestDelete_Click(object sender, RoutedEventArgs e)
        {
            if (comboBoxTests.SelectedItem != null)
            {
                string deleteTestName = comboBoxTests.SelectedItem.ToString();
                if (MessageBox.Show($"Вы действительно хотите удалить тест:\n{deleteTestName}?", "", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    testsCollection.DeleteTest(deleteTestName);
                    comboBoxTests.ItemsSource = testsCollection.GetStudentCollectionToString();
                }
            }

        }
        //Редактировать TEST
        private void buttonTestEdit_Click(object sender, RoutedEventArgs e)
        {
            if (comboBoxTests.SelectedItem != null)
            {
                AddAndEditTestWindow addAndEditTestWindow = new AddAndEditTestWindow(testsCollection.At(comboBoxTests.SelectedItem.ToString()), user);

                if (addAndEditTestWindow.ShowDialog() == true)
                {
                    Test newTest = addAndEditTestWindow.GetTest();
                    testsCollection.UpdateTest(newTest);
                    comboBoxTests.ItemsSource = testsCollection.GetStudentCollectionToString();
                }
            }
        }
        //Добавить TEST
        private void buttonTestAdd_Click(object sender, RoutedEventArgs e)
        {
            AddAndEditTestWindow addAndEditTestWindow = new AddAndEditTestWindow(user);

            if (addAndEditTestWindow.ShowDialog() == true)
            {
                Test newTest = addAndEditTestWindow.GetNewTest();
                newTest.IdTeacher = user.Id;
                newTest.CountQuestions = 0;
                testsCollection.AddTest(newTest);
                comboBoxTests.ItemsSource = testsCollection.GetStudentCollectionToString();

                testsCollection.Reset(App.DBManager, user);
            }
        }
        //--Привязка выбранного теста к вьюхе при выборе в комбобоксе (вывод вопросов)
        private void comboBoxTests_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (comboBoxTests.SelectedItem != null)
            {
                Test curentTest = testsCollection.At(comboBoxTests.SelectedItem.ToString());
                testViewModel = new TestViewModel(curentTest);
                ListViewQuestions.DataContext = testViewModel;
                ListViewDistractions.DataContext = null;
            }

        }
        //--Привязка выбранного вопроса к вьюхе при выборе в листвью (вывод дистров)
        private void ListViewQuestions_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Question selectedQuestion = ListViewQuestions.SelectedItem as Question;
            if (selectedQuestion != null)
            {
                DistractorViewModel distractorViewModel = new DistractorViewModel(selectedQuestion);
                ListViewDistractions.DataContext = distractorViewModel;
            }
        }
        //--обновляет поля
        private void RefreshQuestions()
        {
            testsCollection.Reset(App.DBManager, user);
            if (comboBoxTests.SelectedItem != null)
            {
                ListViewQuestions.DataContext = null;
                Test curentTest = testsCollection.At(comboBoxTests.SelectedItem.ToString());
                testViewModel = new TestViewModel(curentTest);
                ListViewQuestions.DataContext = testViewModel;
            }
        }
        #region Questions

        //--Кнопка "добавить вопрос"
        private void btnQuestAdd_Click(object sender, RoutedEventArgs e)
        {
            AddAndEditQuestWindow addAndEditQuestWindow = new AddAndEditQuestWindow();

            if (addAndEditQuestWindow.ShowDialog() == true)
            {

                Question newQuestion = addAndEditQuestWindow.getQuest();
                newQuestion.IdTest = testsCollection.At(comboBoxTests.Text).Id;
                testsCollection.At(comboBoxTests.Text).QuestionCollection.Question.Add(newQuestion);
                App.DBManager.AddQuesAndGetId(ref newQuestion);
                RefreshQuestions();
            }
        }
        //--Кнопка "редактировать вопрос"
        private void btnQuestEdit_Click(object sender, RoutedEventArgs e)
        {
            Question editQuestion = ListViewQuestions.SelectedItem as Question;
            AddAndEditQuestWindow addAndEditQuestWindow = new AddAndEditQuestWindow(editQuestion);

            if (addAndEditQuestWindow.ShowDialog() == true)
            {
                //editQuestion = addAndEditQuestWindow.getQuest();
                testsCollection.UpdateQuest(editQuestion);
                testViewModel.Refresh(testsCollection.At(editQuestion.IdTest));
                if (comboBoxTests.SelectedItem != null)
                {
                    ListViewQuestions.DataContext = null;
                    Test curentTest = testsCollection.At(comboBoxTests.SelectedItem.ToString());
                    testViewModel = new TestViewModel(curentTest);
                    ListViewQuestions.DataContext = testViewModel;
                    RefreshQuestions();
                }
            }
        }
        //--Кнопка "Удалить вопрос"
        private void btnQuestDelete_Click(object sender, RoutedEventArgs e)
        {
            Question editQuestion = ListViewQuestions.SelectedItem as Question;
            if (editQuestion != null && MessageBox.Show(
                    $"Вы действительно хотите удалить вопрос №{editQuestion.Number}:\n{editQuestion.Text}?",
                    "", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                testsCollection.At(editQuestion.IdTest).QuestionCollection.DeleteQuest(editQuestion);
                RefreshQuestions();
            }  
        }

        #endregion

        #region Distractors

        //--Кнопка "добавить дистрактор"
        private void btnDistrAdd_Click(object sender, RoutedEventArgs e)
        {
            Distractor newDistractor = null;
            AddAndEditDistractor addAndEditTestWindow = new AddAndEditDistractor(newDistractor);

            if (addAndEditTestWindow.ShowDialog() == true)
            {
                newDistractor = addAndEditTestWindow.GetDistractor();
                Question currentQuest = ListViewQuestions.SelectedItem as Question;
                if (currentQuest == null) return;
                newDistractor.IdQuest = currentQuest.Id;
                currentQuest.AddDistr(newDistractor);

                //comboBoxTests.ItemsSource = testsCollection.GetStudentCollectionToString();
            }
        }
        //--Кнопка "удалить дистрактор"
        private void btnDistrDelete_Click(object sender, RoutedEventArgs e)
        {

            Distractor deleteDistractor = ListViewDistractions.SelectedItem as Distractor;

            if (deleteDistractor != null && MessageBox.Show($"Вы действительно хотите удалить вариант ответа №{deleteDistractor.Number}:\n{deleteDistractor.Text}?", "", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                Question currentQuest = ListViewQuestions.SelectedItem as Question;
                currentQuest.DeleteDistr(deleteDistractor);
            }

        }
        //--Кнопка "редактировать дистрактор"
        private void btnDistrEdit_Click(object sender, RoutedEventArgs e)
        {
            Distractor editDistractor = ListViewDistractions.SelectedItem as Distractor; ;
            AddAndEditDistractor addAndEditTestWindow = new AddAndEditDistractor(editDistractor);

            if (addAndEditTestWindow.ShowDialog() == true)
            {
                editDistractor = addAndEditTestWindow.GetDistractor();
                Question currentQuest = ListViewQuestions.SelectedItem as Question;
                if (currentQuest == null) return;
                currentQuest.UpdateDistr(editDistractor);

                //comboBoxTests.ItemsSource = testsCollection.GetStudentCollectionToString();
            }
        }



        #endregion

        #endregion

        #region TestResult

        private void ComboBoxTestResultSelectedChanged(object sender, SelectionChangedEventArgs e)
        {
            bool byGroup = ComboBoxTestResultGroup.SelectedItem != null;
            bool byTest = ComboBoxTestResultTest.SelectedItem != null;

            if (byGroup && byTest)
            {
                string group = ComboBoxTestResultGroup.SelectedItem.ToString() ?? String.Empty;
                string test = ComboBoxTestResultTest.SelectedItem.ToString() ?? String.Empty;

                testResultCollection.GetTestResultByGroupAndTest(groupCollection.GetId(group), testsCollection.GetId(test), App.DBManager);
            }
            else if (byGroup)
            {
                string group = ComboBoxTestResultGroup.SelectedItem.ToString() ?? String.Empty;
                testResultCollection.GetTestResultByGroup(groupCollection.GetId(group), App.DBManager);
            }
            else if (byTest)
            {
                string test = ComboBoxTestResultTest.SelectedItem.ToString() ?? String.Empty;
                testResultCollection.GetTestResultByTest(testsCollection.GetId(test), App.DBManager);
            }

            if (byGroup || byTest)
            {
                DataGridTestResult.DataContext = null;
                DataGridTestResult.DataContext = testResultViewModel;
            }
        }


        #endregion

        //--печать
        private void BtnPrint_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Controls.PrintDialog Printdlg = new System.Windows.Controls.PrintDialog();
            if ((bool)Printdlg.ShowDialog().GetValueOrDefault())
            {
                Size pageSize = new Size(Printdlg.PrintableAreaWidth, Printdlg.PrintableAreaHeight);
                // sizing of the element.
                DataGridTestResult.Measure(pageSize);
                DataGridTestResult.Arrange(new Rect(5, 5, pageSize.Width, pageSize.Height));
                Printdlg.PrintVisual(DataGridTestResult, Title);
            }
        }
    }




}
