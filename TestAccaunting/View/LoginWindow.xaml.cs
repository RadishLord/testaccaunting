﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TestAccaunting.ProgramEntrance;

namespace TestAccaunting.View
{
    /// <summary>
    /// Логика взаимодействия для LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        //  --  declare --  //
        Status status;
        User user;
        string password;
        //

        public LoginWindow()
        {
            InitializeComponent();
            status = new Status(ref StatusBarLogin);
            user = new User();
            password = string.Empty;
        }


        //--Событие "Открытие формы"
        private void Window_SourceInitialized(object sender, EventArgs e)
        {
            //--Проверка соединения с БД
            if (App.DBManager.CheckConnectionToDB())
            {

                status.Set("Соединение установленно.", Brushes.Green);
            }
            else
            {
                status.Set("Ошибка соединения.", Brushes.Red);
            }

            MainGrid.DataContext = user;
        }

        //--Кнопка "Вход"
        private void btEnterIn_Click(object sender, RoutedEventArgs e)
        {
            Authentication authentication = new Authentication();       //--Экземпляр аунтефикатора
            if (authentication.IsSuccess(user, App.DBManager))          //--Проверка введенных данных юзера через DBManager
            {
                status.Set("Доступ разрешен", Brushes.Green);           //--Вход в программу
                user = authentication.GetUserData(user, App.DBManager);
                MainWindow mainWindow = new MainWindow(user);
                mainWindow.Show();
                this.Close();
            }
            else
            {
                status.AuthenticationErrorDisplay(authentication.LoginIsCorrect, authentication.PasswordIsCorrect);
                if (!authentication.LoginIsCorrect)
                {
                    popupLoginError.IsOpen = true;
                }
                else if (!authentication.PasswordIsCorrect)
                {
                    popupPassError.IsOpen = true;
                }
            }

        }
        //--Кнопка "Регистрация"
        private void btRegistrationReg_Click(object sender, RoutedEventArgs e)
        {
            //MessageBox.Show($"{user.Login} - {user.Password} - {user.FIO}");
            bool access = true;
            //--Проверка на длину логина
            if (tbLoginReg.Text.Length < 5)
            {
                status.Set("Логин должен содержать более 5 символов!", Brushes.Red);
                popupLoginLenghlError.IsOpen = true;
                access = false;
            }
            //--Проверка на сопадения пароля
            if (!string.Equals(pbPass1Reg.Text, pbPass2Reg.Text))
            {
                popupPassEqualError1.IsOpen = true;
                popupPassEqualError2.IsOpen = true;
                access = false;
            }
            else
            {
                //--Проверка на длину пароля
                if (pbPass1Reg.Text.Length < 5)
                {
                    status.Set("Пароль должен содержать более 5 символов!", Brushes.Red);
                    popupPassLenghlError.IsOpen = true;
                    access = false;
                }
            }
            //--Проверка фио
            if (string.IsNullOrWhiteSpace(tbFioReg.Text))
            {
                status.Set("Введите ФИО!", Brushes.Red);
                popupFIOlError2.IsOpen = true;
                access = false;
            }
            //--Проверка кода 
            if (!string.Equals(tbCodeReg.Text, RegistrationCode.GetCode))
            {
                popupRegCodeError.IsOpen = true;
                access = false;
            }
            if (access)
            {
                Registration registration = new Registration();
                if (registration.IsSuccess(user, App.DBManager))
                {
                    status.Set("Зарегистрирован", Brushes.Green);           //--Вход в программу
                    MainWindow mainWindow = new MainWindow(user);
                    mainWindow.Show();
                    this.Close();
                }
                else
                {
                    status.RegistrationErrorDisplay(registration.LoginIsCorrect, registration.PasswordIsCorrect);
                }
            }
            else
            {
                status.Set("Ошибка регистрации", Brushes.Red);
            }
            
        }

        //--переключение между авторизаций и регистрацией
        private void btRegIn_Click(object sender, RoutedEventArgs e)
        {
            LoginTabControl.SelectedIndex = 0;
            LoginTabControl.SelectedItem = TabItem2;

        }
        private void btEnterReg_Click(object sender, RoutedEventArgs e)
        {
            LoginTabControl.SelectedIndex = 1;
            LoginTabControl.SelectedItem = TabItem1;
        }
        //--не пропускает указанные символы
        private void TextFilter(object sender, TextCompositionEventArgs e)
        {
           if (e.Handled = "(){}[]!.,;$@#^&*+=\\||/<>".IndexOf(e.Text) > 0)
            {
                popupErrorSymbol.PlacementTarget = sender as UIElement;
                popupErrorSymbol.Placement = System.Windows.Controls.Primitives.PlacementMode.Bottom;
                popupErrorSymbol.IsOpen = true;
                
            }
        }

    }
}
