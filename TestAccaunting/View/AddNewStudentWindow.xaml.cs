﻿using System;
using System.Windows;
using TestAccaunting.Data;

namespace TestAccaunting.View
{
    /// <summary>
    /// Логика взаимодействия для AddNewStudentWindow.xaml
    /// </summary>
    public partial class AddNewStudentWindow : Window
    {
        public AddNewStudentWindow(GroupCollection groupList)
        {
            InitializeComponent();
            ComboBoxGroupNewStudent.ItemsSource = groupList.GetGroupCollectionToString(null,null);
        }
        public Student GetStudent()
        {
            return new Student(TextBoxFio.Text,TextBoxPass.Text,TextBoxLogin.Text, ComboBoxGroupNewStudent.SelectedItem.ToString());
        }

        private void ButtonOK_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(TextBoxFio.Text) &&
                !string.IsNullOrWhiteSpace(ComboBoxGroupNewStudent.SelectedItem.ToString()))
                this.DialogResult = true;
            else
            {
                MessageBox.Show("Данные введены некорректно!");
            }
        }

        private void chPassAuto_Checked(object sender, RoutedEventArgs e)
        {

            TextBoxPass.IsEnabled = (bool) !chPassAuto.IsChecked;
            TextBoxPass.Text = String.Empty;
        }


        private void chLoginAuto_Click(object sender, RoutedEventArgs e)
        {
            TextBoxLogin.IsEnabled = (bool)!chLoginAuto.IsChecked;
            TextBoxLogin.Text = String.Empty;
        }
    }
}
