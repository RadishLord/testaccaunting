﻿using System.Globalization;
using System.Windows;
using TestAccaunting.Data;

namespace TestAccaunting.View
{
    /// <summary>
    /// Логика взаимодействия для AddAndEditQuestWindow.xaml
    /// </summary>
    public partial class AddAndEditQuestWindow : Window
    {
        private Question _quest;

        public AddAndEditQuestWindow()
        {
            InitializeComponent();
            _quest = new Question();
        }
        public AddAndEditQuestWindow(Question quest)
        {
            _quest = quest;
            InitializeComponent();
            tbText.Text = _quest.Text;
            tbNumber.Text = _quest.Number.ToString();
            tbCost.Text = _quest.Cost.ToString(CultureInfo.InvariantCulture);
            btnAdd.Content = "Сохранить";
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {

            int number;
            float cost;
            if (!string.IsNullOrWhiteSpace(tbNumber.Text) &&
                !string.IsNullOrWhiteSpace(tbCost.Text) &&
                !string.IsNullOrWhiteSpace(tbText.Text) &&
                int.TryParse(tbNumber.Text, out number) &&
                float.TryParse(tbCost.Text, out cost))
            {
                _quest.Text = tbText.Text;
                _quest.Number = number;
                _quest.Cost = cost;
                this.DialogResult = true;
            }
            else
            {
                MessageBox.Show("Данные введены некорректно!");
            }
        }

        public Question getQuest()
        {
            return _quest;
        }


    }
}
