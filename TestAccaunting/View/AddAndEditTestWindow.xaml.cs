﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TestAccaunting.Data;

namespace TestAccaunting.View
{
    /// <summary>
    /// Логика взаимодействия для AddAndEditTestWindow.xaml
    /// </summary>
    public partial class AddAndEditTestWindow : Window
    {
        Test _test;
        private User _user;
        public AddAndEditTestWindow(Test test, User user)
        {
            _user = user;
            InitializeComponent();
            _test = test;
            TextBoxName.Text = _test.Name;
        }

        public AddAndEditTestWindow(User user)
        {
            _user = user;
            InitializeComponent();
        }

        public Test GetTest()
        {
            _test.Name =TextBoxName.Text;
            return _test;
        }
        public Test GetNewTest()
        {
            Test test = new Test(_user.Id);
            test.Name = TextBoxName.Text;
            
            return test;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(TextBoxName.Text))
                this.DialogResult = true;
            else
                MessageBox.Show("Введите значение");
        }
    }
}
