﻿using System;
using System.Windows;
using TestAccaunting.Data;

namespace TestAccaunting.View
{
    /// <summary>
    /// Логика взаимодействия для AddNewGroupWindow.xaml
    /// </summary>
    public partial class AddNewGroupWindow : Window
    {
        public AddNewGroupWindow()
        {
            InitializeComponent();
            CathedraBox.ItemsSource = App.DBManager.GetAllCathedra();
        }

        private void Accept_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(NameBox.Text) && !string.IsNullOrWhiteSpace(CathedraBox.Text) && !string.IsNullOrWhiteSpace(datePicker.Text))
                this.DialogResult = true;
            else
            {
                MessageBox.Show("Данные введены некорректно!");
            }
        }

        public string NewGroupName
        {
            get { return NameBox.Text; }
        }
        public string NewGroupCathedra
        {
            get { return CathedraBox.Text; }
        }
        public DateTime? NewGroupYear
        {
            get { return datePicker.SelectedDate; }
        }
        public Group GetNewGroup()
        {
            Group group = new Group();
            group.Name = NameBox.Text;
            group.Cathedra = CathedraBox.Text;
            group.RecDate = datePicker.SelectedDate;
            return group;
        }
    }
}
