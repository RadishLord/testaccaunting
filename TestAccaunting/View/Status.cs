﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;

namespace TestAccaunting.View
{
    public class Status : INotifyPropertyChanged
    {
        //  --  Выводит на экран строку Str цветом Color

        public Status(ref TextBlock textBlock)
        {
            textBlock.DataContext = this;
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, e);
        }

        private string str;    
        public string Str
        {
            get { return str; }
            set
            {
                str = value;
                OnPropertyChanged(new PropertyChangedEventArgs("str"));
            }
        }

        private Brush color;
        public Brush Color
        {
            get { return color; }
            set
            {
                color = value;
                OnPropertyChanged(new PropertyChangedEventArgs("color"));
            }
        }

        public void Set(string message, Brush br)
        {
            Str = message;
            Color = br;
        }

        public void AuthenticationErrorDisplay(bool loginIsCorrect, bool passwordIsCorrect)
        {
            if(!loginIsCorrect)
            {
                this.Set("Неверный логин!", Brushes.Red);
            }
            else if (!passwordIsCorrect)
            {
                this.Set("Неверный пароль!", Brushes.Red);
            }
        }

        internal void RegistrationErrorDisplay(bool loginIsCorrect, bool passwordIsCorrect)
        {
            if (!loginIsCorrect)
            {
                this.Set("Ошибка Регистрации! Указанный логин уже существует!", Brushes.Red);
            }
            else if (!passwordIsCorrect)
            {
                this.Set("Ошибка Регистрации! Ошибка пароля!", Brushes.Red);
            }
        }
    }
}
