﻿using System.Windows;
using TestAccaunting.Data;

namespace TestAccaunting.View
{
    /// <summary>
    /// Логика взаимодействия для AddAndEditDistractor.xaml
    /// </summary>
    public partial class AddAndEditDistractor : Window
    {
        private Distractor _distr;
        public AddAndEditDistractor(Distractor distractor)
        {
            InitializeComponent();
            _distr = distractor;

            if (_distr != null)
            {
                tbText.Text = _distr.Text;
                tbNumber.Text = _distr.Number.ToString();
                ChBxIsRight.IsChecked = _distr.IsRight;
            }
            else
            {
                _distr = new Distractor();
            }
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(tbNumber.Text) &&
                !string.IsNullOrWhiteSpace(tbText.Text) &&
                int.TryParse(tbNumber.Text, out var number))
            {

                _distr.Text = tbText.Text;
                _distr.Number = number;
                _distr.IsRight = ChBxIsRight.IsChecked != null && (bool) ChBxIsRight.IsChecked;

                this.DialogResult = true;
            }
            else
            {
                MessageBox.Show("Данные введены некорректно!");
            }
        }

        public Distractor GetDistractor()
        {
            return _distr;
        }
    }
}
