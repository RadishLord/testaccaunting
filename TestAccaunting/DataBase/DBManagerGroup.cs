﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using TestAccaunting.Data;

namespace TestAccaunting.DataBase
{
    
    public partial class DBManager
    {
        //--Чать DBManager ответственная за Группы

        /// <summary>
        /// Обновляет указанную группу в БД
        /// </summary>
        /// <param name="group"></param>
        internal void UpdateGroup(Group group)
        {
            try
            {
                NpgsqlConnection bd = new NpgsqlConnection(connectionString);
                bd.Open();

                NpgsqlCommand npgSqlCommand = new NpgsqlCommand($"SELECT group_update('{group.Id}','{group.Name}','{group.Cathedra}','{group.RecDate.ToString()}');", bd);
                npgSqlCommand.ExecuteNonQuery();
                bd.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("ex:" + ex.Message);
            }
        }
        /// <summary>
        /// Возвращает коллекцию групп
        /// </summary>
        /// <returns></returns>
        internal ObservableCollection<Group> GetAllGroup()
        {
            ObservableCollection<Group> ListGroup = new ObservableCollection<Group>();
            try
            {
                NpgsqlConnection bd = new NpgsqlConnection(connectionString);
                bd.Open();
                NpgsqlCommand npgSqlCommand = new NpgsqlCommand($"SELECT * FROM get_all_group();", bd);

                NpgsqlDataReader npgSqlDataReader = npgSqlCommand.ExecuteReader();

                if (npgSqlDataReader.HasRows)                                                                                   //--если такой логин есть в бд
                {
                    foreach (DbDataRecord rec in npgSqlDataReader)
                    {
                        ListGroup.Add(new Group(rec.GetInt32(0), string.Empty, rec.GetString(1), rec.GetString(2), rec.GetInt32(3), rec.GetDateTime(4), false));
                    }
                }
                bd.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("ex:" + ex.Message);
            }

            return ListGroup;

        }
        /// <summary>
        /// Перезаполняет коллекцию групп
        /// </summary>
        /// <param name="collection"></param>
        internal void GetAllGroup(ref ObservableCollection<Group> collection)
        {
            collection.Clear();
            try
            {
                NpgsqlConnection bd = new NpgsqlConnection(connectionString);
                bd.Open();
                NpgsqlCommand npgSqlCommand = new NpgsqlCommand($"SELECT * FROM get_all_group();", bd);

                NpgsqlDataReader npgSqlDataReader = npgSqlCommand.ExecuteReader();

                if (npgSqlDataReader.HasRows)                                                                                   //--если такой логин есть в бд
                {
                    foreach (DbDataRecord rec in npgSqlDataReader)
                    {
                        collection.Add(new Group(rec.GetInt32(0), string.Empty, rec.GetString(1), rec.GetString(2), rec.GetInt32(3), rec.GetDateTime(4), false));
                    }
                }
                bd.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("ex:" + ex.Message);
            }
        }
        /// <summary>
        /// Удаляет группу по id в БД
        /// </summary>
        /// <param name="id"></param>
        internal void DeleteGroup(int id)
        {
            try
            {
                NpgsqlConnection bd = new NpgsqlConnection(connectionString);
                bd.Open();

                NpgsqlCommand npgSqlCommand = new NpgsqlCommand($"SELECT study_group_delete('{id}');", bd);
                npgSqlCommand.ExecuteNonQuery();
                bd.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("ex:" + ex.Message);
            }
        }
        /// <summary>
        /// Добавляет группу по id в БД
        /// </summary>
        /// <param name="group"></param>
        internal void AddGroup(Group group)
        {
            try
            {
                NpgsqlConnection bd = new NpgsqlConnection(connectionString);
                bd.Open();

                NpgsqlCommand npgSqlCommand = new NpgsqlCommand($"SELECT add_group('{group.Name}','{group.Cathedra}','{group.RecDate.ToString()}');", bd);
                npgSqlCommand.ExecuteNonQuery();
                bd.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("ex:" + ex.Message);
            }
        }
        /// <summary>
        /// Возвращает список кафедр
        /// </summary>
        /// <returns></returns>
        internal List<string> GetAllCathedra()
        {
            List<string> CathdraList = new List<string>();
            try
            {
                NpgsqlConnection bd = new NpgsqlConnection(connectionString);
                bd.Open();
                NpgsqlCommand npgSqlCommand = new NpgsqlCommand($"SELECT get_all_cathedra();", bd);

                NpgsqlDataReader npgSqlDataReader = npgSqlCommand.ExecuteReader();

                if (npgSqlDataReader.HasRows)                                                                                   //--если такой логин есть в бд
                {
                    foreach (DbDataRecord dbDataRecord in npgSqlDataReader)
                    {
                        CathdraList.Add(dbDataRecord.GetString(0));
                    }
                }
                bd.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("ex:" + ex.Message);
            }

            return CathdraList;
        }

        public bool CheckGroupReferences(int delGroupId)
        {
            bool res = false;
            try
            {
                NpgsqlConnection bd = new NpgsqlConnection(connectionString);
                bd.Open();
                string query = $"SELECT check_group_references('{delGroupId}');";
                NpgsqlCommand npgSqlCommand = new NpgsqlCommand(query, bd);

                NpgsqlDataReader npgSqlDataReader = npgSqlCommand.ExecuteReader();

                if (npgSqlDataReader.HasRows)                                                                                   //--если такой логин есть в бд
                {
                    foreach (DbDataRecord rec in npgSqlDataReader)
                    {
                        res = rec.GetBoolean(0);
                    }
                }
                bd.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("CheckReferences ex:" + ex.Message);
            }

            return res;
        }
    }
}
