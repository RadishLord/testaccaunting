﻿using System;
using System.Collections.ObjectModel;
using System.Data.Common;
using System.Windows;
using Npgsql;
using TestAccaunting.Data;

namespace TestAccaunting.DataBase
{
    public partial class DBManager
    {
        internal ObservableCollection<TestResult> GetTestResult()
        {
            ObservableCollection<TestResult> testResults = new ObservableCollection<TestResult>();
            try
            {
                NpgsqlConnection bd = new NpgsqlConnection(connectionString);
                bd.Open();
                NpgsqlCommand npgSqlCommand = new NpgsqlCommand($"SELECT * FROM get_test_result();", bd);

                NpgsqlDataReader npgSqlDataReader = npgSqlCommand.ExecuteReader();

                if (npgSqlDataReader.HasRows)                                                                                   //--если такой логин есть в бд
                {
                    foreach (DbDataRecord rec in npgSqlDataReader)
                    {
                        testResults.Add(new TestResult(rec.GetString(0), 
                                                        rec.GetString(1),
                                                        rec.GetString(2),
                                                        rec.GetInt32(3), 
                                                        rec.GetInt32(4),
                                                        rec.GetInt32(5),
                                                        rec.GetInt32(6),
                                                        rec.GetInt32(7),
                                                        rec.GetInt32(8)));
                    }
                }
                bd.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("ex:" + ex.Message);
            }


            return testResults;
        }


        public void GetTestResultByGroupAndTest(ref ObservableCollection<TestResult> testResults, int idGroup, int idTest)
        {
            try
            {
                NpgsqlConnection bd = new NpgsqlConnection(connectionString);
                bd.Open();
                NpgsqlCommand npgSqlCommand = new NpgsqlCommand($"SELECT * FROM get_test_result_by_group_and_test('{idGroup}','{idTest}');", bd);

                NpgsqlDataReader npgSqlDataReader = npgSqlCommand.ExecuteReader();

                if (npgSqlDataReader.HasRows)                                                                                   //--если такой логин есть в бд
                {
                    foreach (DbDataRecord rec in npgSqlDataReader)
                    {
                        testResults.Add(new TestResult(rec.GetString(0),
                            rec.GetString(1),
                            rec.GetString(2),
                            rec.GetInt32(3),
                            rec.GetInt32(4),
                            rec.GetInt32(5),
                            rec.GetInt32(6),
                            rec.GetInt32(7),
                            rec.GetInt32(8)));
                    }
                }
                bd.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("ex:" + ex.Message);
            }
        }

        public void GetTestResultByGroup(ref ObservableCollection<TestResult> testResults, int IdGroup)
        {
            try
            {
                NpgsqlConnection bd = new NpgsqlConnection(connectionString);
                bd.Open();
                NpgsqlCommand npgSqlCommand = new NpgsqlCommand($"SELECT * FROM get_test_result_by_group('{IdGroup}');", bd);

                NpgsqlDataReader npgSqlDataReader = npgSqlCommand.ExecuteReader();

                if (npgSqlDataReader.HasRows)                                                                                   //--если такой логин есть в бд
                {
                    foreach (DbDataRecord rec in npgSqlDataReader)
                    {
                        testResults.Add(new TestResult(rec.GetString(0),
                            rec.GetString(1),
                            rec.GetString(2),
                            rec.GetInt32(3),
                            rec.GetInt32(4),
                            rec.GetInt32(5),
                            rec.GetInt32(6),
                            rec.GetInt32(7),
                            rec.GetInt32(8)));
                    }
                }
                bd.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("ex:" + ex.Message);
            }
        }

        public void GetTestResultByTest(ref ObservableCollection<TestResult> testResults, int IdTest)
        {
         
            try
            {
                NpgsqlConnection bd = new NpgsqlConnection(connectionString);
                bd.Open();
                NpgsqlCommand npgSqlCommand = new NpgsqlCommand($"SELECT * FROM get_test_result_by_test('{IdTest}');", bd);

                NpgsqlDataReader npgSqlDataReader = npgSqlCommand.ExecuteReader();

                if (npgSqlDataReader.HasRows)                                                                                   //--если такой логин есть в бд
                {
                    foreach (DbDataRecord rec in npgSqlDataReader)
                    {
                        testResults.Add(new TestResult(rec.GetString(0),
                            rec.GetString(1),
                            rec.GetString(2),
                            rec.GetInt32(3),
                            rec.GetInt32(4),
                            rec.GetInt32(5),
                            rec.GetInt32(6),
                            rec.GetInt32(7),
                            rec.GetInt32(8)));
                    }
                }
                bd.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("GetTestResultByTest ex:" + ex.Message);
            }



        }
    }
}