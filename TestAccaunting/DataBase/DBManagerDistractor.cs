﻿using System;
using System.Data.Common;
using System.Windows;
using Npgsql;
using TestAccaunting.Data;

namespace TestAccaunting.DataBase
{
    public partial class DBManager
    {

        internal void AddDistractor(Distractor dist)
        {
            try
            {
                NpgsqlConnection bd = new NpgsqlConnection(connectionString);
                bd.Open();
                //i_id_test int, i_text character varying, i_number_quest int, i_cost real)
                NpgsqlCommand npgSqlCommand = new NpgsqlCommand($"SELECT add_distr('{dist.Number}','{dist.Text}','{dist.IsRight}','{dist.IdQuest}');", bd);
                npgSqlCommand.ExecuteNonQuery();
                bd.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("AddDistractor ex:" + ex.Message);
            }
        }

        internal void AddDistrAndReturnId(ref Distractor distr)
        {
            try
            {

                NpgsqlConnection bd = new NpgsqlConnection(connectionString);
                bd.Open();
                //i_id_test int, i_text character varying, i_number_quest int, i_cost real)
                NpgsqlCommand npgSqlCommand = new NpgsqlCommand($"SELECT add_distr_and_return_id('{distr.Number}', '{distr.Text}', '{distr.IsRight}', '{distr.IdQuest}');", bd);


                NpgsqlDataReader npgSqlDataReader = npgSqlCommand.ExecuteReader();

                if (npgSqlDataReader.HasRows)                                                                                   //--если такой логин есть в бд
                {
                    foreach (DbDataRecord rec in npgSqlDataReader)
                    {
                        distr.Id = rec.GetInt32(0);
                    }
                }
                bd.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show("AddQuesAndReturnId ex:" + ex.Message);
            }
        }

        internal void UpdateDistractor(Distractor dist)
        {
            try
            {
                NpgsqlConnection bd = new NpgsqlConnection(connectionString);
                bd.Open();
                
                NpgsqlCommand npgSqlCommand = new NpgsqlCommand($"SELECT update_distr('{dist.Id}','{dist.Number}','{dist.Text}','{dist.IsRight}','{dist.IdQuest}');", bd);
                npgSqlCommand.ExecuteNonQuery();
                bd.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("UpdateQuestion ex:" + ex.Message);
            }
        }

        public void DeleteDistractor(Distractor dist)
        {
            try
            {
                NpgsqlConnection bd = new NpgsqlConnection(connectionString);
                bd.Open();

                NpgsqlCommand npgSqlCommand = new NpgsqlCommand($"SELECT delete_distr('{dist.Id}');", bd);
                npgSqlCommand.ExecuteNonQuery();
                bd.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("DeleteDistractor ex:" + ex.Message);
            }
        }
    }
}