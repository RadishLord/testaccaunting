﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using TestAccaunting.Data;

namespace TestAccaunting.DataBase
{
    public partial class DBManager
    {
        //--Чать DBManager ответственная за Студентов

        /// <summary>
        /// Возвращает коллекцию студентов из БД
        /// </summary>
        /// <returns></returns>
        internal ObservableCollection<Student> GetAllStudents()
        {
            ObservableCollection<Student> ListStudent = new ObservableCollection<Student>();
            try
            {
                NpgsqlConnection bd = new NpgsqlConnection(connectionString);
                bd.Open();
                NpgsqlCommand npgSqlCommand = new NpgsqlCommand($"SELECT * FROM get_all_student();", bd);

                NpgsqlDataReader npgSqlDataReader = npgSqlCommand.ExecuteReader();

                if (npgSqlDataReader.HasRows)                                                                                   //--если такой логин есть в бд
                {
                    foreach (DbDataRecord rec in npgSqlDataReader)
                    {
                        ListStudent.Add(new Student(rec.GetInt32(0), rec.GetInt32(1), rec.GetInt32(2), rec.GetString(3), rec.GetString(4), rec.GetString(5), rec.GetString(6), rec.GetString(7), false));
                    }
                }
                bd.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("ex:" + ex.Message);
            }

            return ListStudent;
        }
        /// <summary>
        /// Перезаписывает коллекцию студентов
        /// </summary>
        /// <param name="studentList"></param>
        internal void GetAllStudents(ref ObservableCollection<Student> studentList)
        {
            studentList.Clear();
            try
            {
                NpgsqlConnection bd = new NpgsqlConnection(connectionString);
                bd.Open();
                NpgsqlCommand npgSqlCommand = new NpgsqlCommand($"SELECT * FROM get_all_student();", bd);

                NpgsqlDataReader npgSqlDataReader = npgSqlCommand.ExecuteReader();

                if (npgSqlDataReader.HasRows)                                                                                   //--если такой логин есть в бд
                {
                    foreach (DbDataRecord rec in npgSqlDataReader)
                    {
                        studentList.Add(new Student(rec.GetInt32(0), rec.GetInt32(1), rec.GetInt32(2), rec.GetString(3), rec.GetString(4), rec.GetString(5), rec.GetString(6), rec.GetString(7), false));
                    }
                }
                bd.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("ex:" + ex.Message);
            }
        }
        /// <summary>
        /// Удаляет студента по id из БД
        /// </summary>
        /// <param name="id"></param>
        internal void DeleteStudent(int id)
        {
            try
            {
                NpgsqlConnection bd = new NpgsqlConnection(connectionString);
                bd.Open();

                NpgsqlCommand npgSqlCommand = new NpgsqlCommand($"SELECT delete_student('{id}');", bd);
                npgSqlCommand.ExecuteNonQuery();
                bd.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("ex:" + ex.Message);
            }
        }
        /// <summary>
        /// Добавляет указанного студента в БД логин генерируется SQL функцией
        /// </summary>
        /// <param name="student"></param>
        internal void AddStudent(Student student)
        {
            try
            {
                NpgsqlConnection bd = new NpgsqlConnection(connectionString);
                bd.Open();
                
                NpgsqlCommand npgSqlCommand = new NpgsqlCommand($"SELECT add_student('{student.IdGroup}','{student.Fio}','{student.Login}','{student.Passw}');", bd);
                npgSqlCommand.ExecuteNonQuery();
                bd.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("ex:" + ex.Message);
            }
        }
        /// <summary>
        /// Обновляет запись указанного студента по id в БД
        /// </summary>
        /// <param name="student"></param>
        internal void UpdateStudent(Student student)
        {
            try
            {
                NpgsqlConnection bd = new NpgsqlConnection(connectionString);
                bd.Open();

                NpgsqlCommand npgSqlCommand = new NpgsqlCommand($"SELECT update_student('{student.Id}','{student.IdGroup}','{student.Fio}','{student.Login}','{student.Passw}');", bd);
                npgSqlCommand.ExecuteNonQuery();
                bd.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("ex:" + ex.Message);
            }
        }     
        /// <summary>
        /// Возвращает студентов с заданным параметрами
        /// </summary>
        /// <param name="studentList"></param>
        /// <param name="cathedra"></param>
        /// <param name="year"></param>
        internal void GetStudentByCathedraAndYear(ref ObservableCollection<Student> studentList, string cathedra, string year)
        {
            studentList.Clear();
            try
            {
                NpgsqlConnection bd = new NpgsqlConnection(connectionString);
                bd.Open();
                string query = $"SELECT * FROM get_student_by_cathedra_and_course('{cathedra}','{year}');";
                NpgsqlCommand npgSqlCommand = new NpgsqlCommand(query, bd);

                NpgsqlDataReader npgSqlDataReader = npgSqlCommand.ExecuteReader();

                if (npgSqlDataReader.HasRows)                                                                                   //--если такой логин есть в бд
                {
                    foreach (DbDataRecord rec in npgSqlDataReader)
                    {
                        studentList.Add(new Student(rec.GetInt32(0), rec.GetInt32(1), rec.GetInt32(2), rec.GetString(3), rec.GetString(4), rec.GetString(5), rec.GetString(6), rec.GetString(7), false));
                    }
                }
                bd.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("ex:" + ex.Message);
            }
        }
        /// <summary>
        /// Возвращает студентов с заданным параметрами
        /// </summary>
        /// <param name="studentList"></param>
        /// <param name="cathedra"></param>
        /// <param name="year"></param>
        internal void GetStudentByCathedra(ref ObservableCollection<Student> studentList, string cathedra)
        {
            studentList.Clear();
            try
            {
                NpgsqlConnection bd = new NpgsqlConnection(connectionString);
                bd.Open();
                string query = $"SELECT * FROM get_student_by_cathedra('{cathedra}');";
                NpgsqlCommand npgSqlCommand = new NpgsqlCommand(query, bd);

                NpgsqlDataReader npgSqlDataReader = npgSqlCommand.ExecuteReader();

                if (npgSqlDataReader.HasRows)                                                                                   //--если такой логин есть в бд
                {
                    foreach (DbDataRecord rec in npgSqlDataReader)
                    {
                        studentList.Add(new Student(rec.GetInt32(0), rec.GetInt32(1), rec.GetInt32(2), rec.GetString(3), rec.GetString(4), rec.GetString(5), rec.GetString(6), rec.GetString(7), false));
                    }
                }
                bd.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("ex:" + ex.Message);
            }
        }
        /// <summary>
        /// Возвращает студентов с заданным параметрами
        /// </summary>
        /// <param name="studentList"></param>
        /// <param name="cathedra"></param>
        /// <param name="year"></param>
        internal void GetStudentByYear(ref ObservableCollection<Student> studentList, string year)
        {
            studentList.Clear();
            try
            {
                NpgsqlConnection bd = new NpgsqlConnection(connectionString);
                bd.Open();
                NpgsqlCommand npgSqlCommand = new NpgsqlCommand($"SELECT * FROM get_student_by_course('{year}');", bd);

                NpgsqlDataReader npgSqlDataReader = npgSqlCommand.ExecuteReader();

                if (npgSqlDataReader.HasRows)                                                                                   //--если такой логин есть в бд
                {
                    foreach (DbDataRecord rec in npgSqlDataReader)
                    {
                        studentList.Add(new Student(rec.GetInt32(0), rec.GetInt32(1), rec.GetInt32(2), rec.GetString(3), rec.GetString(4), rec.GetString(5), rec.GetString(6), rec.GetString(7), false));
                    }
                }
                bd.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("ex:" + ex.Message);
            }
        }
        /// <summary>
        /// Возвращает студентов с заданным параметрами
        /// </summary>
        /// <param name="studentList"></param>
        /// <param name="cathedra"></param>
        /// <param name="year"></param>
        internal void GetStudentByGroup(ref ObservableCollection<Student> studentList, string group)
        {
            studentList.Clear();
            try
            {
                NpgsqlConnection bd = new NpgsqlConnection(connectionString);
                bd.Open();
                NpgsqlCommand npgSqlCommand = new NpgsqlCommand($"SELECT * FROM get_student_by_group('{group}');", bd);

                NpgsqlDataReader npgSqlDataReader = npgSqlCommand.ExecuteReader();

                if (npgSqlDataReader.HasRows)                                                                                   //--если такой логин есть в бд
                {
                    foreach (DbDataRecord rec in npgSqlDataReader)
                    {
                        studentList.Add(new Student(rec.GetInt32(0), rec.GetInt32(1), rec.GetInt32(2), rec.GetString(3), rec.GetString(4), rec.GetString(5), rec.GetString(6), rec.GetString(7), false));
                    }
                }
                bd.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("ex:" + ex.Message);
            }
        }


        public bool CheckReferences(int deleteStudentId)
        {
            bool res = false;
            try
            {
                NpgsqlConnection bd = new NpgsqlConnection(connectionString);
                bd.Open();
                string query = $"SELECT check_student_references('{deleteStudentId}');";
                NpgsqlCommand npgSqlCommand = new NpgsqlCommand(query, bd);

                NpgsqlDataReader npgSqlDataReader = npgSqlCommand.ExecuteReader();

                if (npgSqlDataReader.HasRows)                                                                                   //--если такой логин есть в бд
                {
                    foreach (DbDataRecord rec in npgSqlDataReader)
                    {
                        res = rec.GetBoolean(0);
                    }
                }
                bd.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("CheckReferences ex:" + ex.Message);
            }

            return res;
        }
    }
}
