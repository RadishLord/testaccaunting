﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace TestAccaunting.DataBase
{
    public partial class DBManager
    {

        /// <summary>
        /// Возвращает истину если логин есть в БД
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public bool UserLoginCheck(User user)
        {
            bool result = false;
            try
            {
                NpgsqlConnection bd = new NpgsqlConnection(connectionString);
                bd.Open();
                NpgsqlCommand npgSqlCommand = new NpgsqlCommand($"SELECT login_check('{user.Login}');", bd);

                NpgsqlDataReader npgSqlDataReader = npgSqlCommand.ExecuteReader();

                if (npgSqlDataReader.HasRows)                                                                                   //--если такой логин есть в бд
                {
                    foreach (DbDataRecord dbDataRecord in npgSqlDataReader)
                    {
                        result = dbDataRecord.GetBoolean(0);
                    }
                }
                bd.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("ex:" + ex.Message);
            }

            return result;
        }


        /// <summary>
        /// Возвращает учетную запись преподавателя (кроме пароля)
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        internal User GetTeacherData(User user)
        {
            try
            {
                NpgsqlConnection bd = new NpgsqlConnection(connectionString);
                bd.Open();
                NpgsqlCommand npgSqlCommand = new NpgsqlCommand($"SELECT * FROM get_teacher_data('{user.Login}','{user.Password}');", bd);

                NpgsqlDataReader npgSqlDataReader = npgSqlCommand.ExecuteReader();

                if (npgSqlDataReader.HasRows)                                                                                   //--если такой логин есть в бд
                {
                    foreach (DbDataRecord dbDataRecord in npgSqlDataReader)
                    {
                        user.FIO = dbDataRecord.GetString(0);
                        user.Id = dbDataRecord.GetInt32(1);
                    }
                }
                bd.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("ex:" + ex.Message);
            }

            return user;
        }


        /// <summary>
        /// Проверка пароля в бд
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public bool UserPassCheck(User user)
        {
            bool result = false;
            try
            {
                NpgsqlConnection bd = new NpgsqlConnection(connectionString);
                bd.Open();
                NpgsqlCommand npgSqlCommand = new NpgsqlCommand($"SELECT passw_check('{user.Login}','{user.Password}');", bd);

                NpgsqlDataReader npgSqlDataReader = npgSqlCommand.ExecuteReader();

                if (npgSqlDataReader.HasRows)                                                                                   //--если такой логин есть в бд
                {
                    foreach (DbDataRecord dbDataRecord in npgSqlDataReader)
                    {
                        if (dbDataRecord.GetBoolean(0))
                        {
                            result = true;
                        }
                    }
                }
                bd.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("ex:" + ex.Message);
            }

            return result;
        }
        /// <summary>
        /// Проверяет логин\пароль в БД
        /// </summary>
        /// <param name="user"></param>
        /// <param name="ConfirmedLogin"></param>
        /// <param name="ConfirmedPassw"></param>
        public void LoginAndPassCheck(User user, out bool ConfirmedLogin, out bool ConfirmedPassw)
        {
            ConfirmedLogin = false;
            ConfirmedPassw = false;
            try
            {
                ConfirmedLogin = UserLoginCheck(user);
                ConfirmedPassw = UserPassCheck(user);
            }
            catch (Exception ex)
            {
                MessageBox.Show("ex:" + ex.Message);
            }
        }
        /// <summary>
        /// Регистрирует преподавателя
        /// </summary>
        /// <param name="user"></param>
        /// <param name="ConfirmedLogin"></param>
        public void RegisterNewTeacher(User user, out bool ConfirmedLogin)
        {
            ConfirmedLogin = !UserLoginCheck(user);
            if (!ConfirmedLogin)
                return;

            try
            {
                NpgsqlConnection bd = new NpgsqlConnection(connectionString);
                bd.Open();

                NpgsqlCommand npgSqlCommand = new NpgsqlCommand($"SELECT teacher_registration('{user.Login}','{user.Password}','{user.FIO}');", bd);
                npgSqlCommand.ExecuteNonQuery();
                bd.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("ex:" + ex.Message);
            }
        }
    }
}
