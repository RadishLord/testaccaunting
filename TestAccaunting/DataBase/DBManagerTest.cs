﻿using Npgsql;
using System;
using System.Collections.ObjectModel;
using System.Data.Common;
using System.Windows;
using TestAccaunting.Data;

namespace TestAccaunting.DataBase
{
    public partial class DBManager
    {
        internal ObservableCollection<Test> GetTestsList(int idTeahcer)
        {
            ObservableCollection<Test> ListTests = new ObservableCollection<Test>();
            try
            {
                NpgsqlConnection bd = new NpgsqlConnection(connectionString);
                bd.Open();
                NpgsqlCommand npgSqlCommand = new NpgsqlCommand($"SELECT * FROM get_tests('{idTeahcer}');", bd);

                NpgsqlDataReader npgSqlDataReader = npgSqlCommand.ExecuteReader();

                if (npgSqlDataReader.HasRows)                                                                                   //--если такой логин есть в бд
                {
                    foreach (DbDataRecord rec in npgSqlDataReader)
                    {
                        ListTests.Add(new Test(rec.GetInt32(0), idTeahcer, rec.GetString(1), rec.GetInt32(2)));
                    }
                }
                bd.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("ex:" + ex.Message);
            }

            return ListTests;
        }

        internal void AddTest(Test test)
        {
            try
            {
                NpgsqlConnection bd = new NpgsqlConnection(connectionString);
                bd.Open();

                NpgsqlCommand npgSqlCommand = new NpgsqlCommand($"SELECT add_test('{test.IdTeacher}','{test.Name}','{test.CountQuestions}');", bd);
                npgSqlCommand.ExecuteNonQuery();
                bd.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("ex:" + ex.Message);
            }
        }

        internal void DeleteTest(int id)
        {
            try
            {
                NpgsqlConnection bd = new NpgsqlConnection(connectionString);
                bd.Open();

                NpgsqlCommand npgSqlCommand = new NpgsqlCommand($"SELECT delete_test('{id}');", bd);
                npgSqlCommand.ExecuteNonQuery();
                bd.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("ex:" + ex.Message);
            }
        }

        internal void UpdateTest(Test test)
        {
            try
            {
                NpgsqlConnection bd = new NpgsqlConnection(connectionString);
                bd.Open();

                NpgsqlCommand npgSqlCommand = new NpgsqlCommand($"SELECT update_test('{test.Id}','{test.IdTeacher}','{test.Name}','{test.CountQuestions}');", bd);
                npgSqlCommand.ExecuteNonQuery();
                bd.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("ex:" + ex.Message);
            }
        }

        internal ObservableCollection<Question> LoadQuestion(int idTest)
        {
            ObservableCollection<Question> questList = new ObservableCollection<Question>();
            try
            {
                NpgsqlConnection bd = new NpgsqlConnection(connectionString);
                bd.Open();
                NpgsqlCommand npgSqlCommand = new NpgsqlCommand($"SELECT * FROM get_questions('{idTest}');", bd);

                NpgsqlDataReader npgSqlDataReader = npgSqlCommand.ExecuteReader();

                if (npgSqlDataReader.HasRows)                                                                                   //--если такой логин есть в бд
                {
                    foreach (DbDataRecord rec in npgSqlDataReader)
                    {
                        Question newQuest = new Question(rec.GetInt32(0), 
                                                    rec.GetInt32(1),
                                                    idTest,
                                                    rec.GetFloat(2),
                                                    rec.GetString(3),
                                                    rec.GetInt32(4));
                        newQuest.LoadDistractors();
                        questList.Add(newQuest);
                    }
                }
                bd.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("LoadQuestion ex:" + ex.Message);
            }

            return questList;
        }

        internal ObservableCollection<Distractor> LoadDistractors(int idQuest)
        {
            ObservableCollection<Distractor> distrList = new ObservableCollection<Distractor>();
            try
            {
                NpgsqlConnection bd = new NpgsqlConnection(connectionString);
                bd.Open();
                NpgsqlCommand npgSqlCommand = new NpgsqlCommand($"SELECT * FROM get_distractors('{idQuest}');", bd);

                NpgsqlDataReader npgSqlDataReader = npgSqlCommand.ExecuteReader();

                if (npgSqlDataReader.HasRows)                                                                                   //--если такой логин есть в бд
                {
                    foreach (DbDataRecord rec in npgSqlDataReader)
                    {
                        distrList.Add(new Distractor(rec.GetInt32(0), rec.GetInt32(1), rec.GetString(2), rec.GetBoolean(3)));
                    }
                }
                bd.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("ex:" + ex.Message);
            }

            return distrList;
        }

    }
}
