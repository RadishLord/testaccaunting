﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Npgsql;
using TestAccaunting.Data;

namespace TestAccaunting.DataBase
{
    public partial class DBManager
    {
        private static string connectionString = Properties.Settings.Default.TestAccauntigDB;

        /// <summary>
        /// Проверяет доступ к БД
        /// </summary>
        /// <returns></returns>
        public bool CheckConnectionToDB()
        {
            NpgsqlConnection npgSqlConnection = new NpgsqlConnection(connectionString);
            try
            {
                npgSqlConnection.Open();
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show($"База данных не доступна!\n\n{ex.Message}");
                return false;
            }
            finally
            {
                npgSqlConnection.Close();
            }
        }



    }
}
