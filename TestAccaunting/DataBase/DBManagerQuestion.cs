﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Npgsql;
using TestAccaunting.Data;

namespace TestAccaunting.DataBase
{
    public partial class DBManager
    {

        internal void AddQuestion(Question quest)
        {
            try
            {
                NpgsqlConnection bd = new NpgsqlConnection(connectionString);
                bd.Open();
                //i_id_test int, i_text character varying, i_number_quest int, i_cost real)
                NpgsqlCommand npgSqlCommand = new NpgsqlCommand($"SELECT add_quest('{quest.IdTest}','{quest.Text}','{quest.Number}','{quest.Cost}');", bd);
                npgSqlCommand.ExecuteNonQuery();
                bd.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("ex:" + ex.Message);
            }
        }

        internal int AddQuesAndReturnId(Question quest)
        {
            int id = 0;
            try
            {
                
                NpgsqlConnection bd = new NpgsqlConnection(connectionString);
                bd.Open();
                //i_id_test int, i_text character varying, i_number_quest int, i_cost real)
                NpgsqlCommand npgSqlCommand = new NpgsqlCommand($"SELECT add_quest_and_return_id('{quest.IdTest}','{quest.Text}','{quest.Number}','{quest.Cost}');", bd);
 

                NpgsqlDataReader npgSqlDataReader = npgSqlCommand.ExecuteReader();

                if (npgSqlDataReader.HasRows)                                                                                   //--если такой логин есть в бд
                {
                    foreach (DbDataRecord rec in npgSqlDataReader)
                    {
                        id = rec.GetInt32(0);
                    }
                }
                bd.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show("AddQuesAndReturnId ex:" + ex.Message);
            }
            return id;
        }

        
        internal void AddQuesAndGetId(ref Question quest)
        {
            try
            {

                NpgsqlConnection bd = new NpgsqlConnection(connectionString);
                bd.Open();
                //i_id_test int, i_text character varying, i_number_quest int, i_cost real)
                NpgsqlCommand npgSqlCommand = new NpgsqlCommand($"SELECT add_quest_and_return_id('{quest.IdTest}','{quest.Text}','{quest.Number}','{quest.Cost}');", bd);


                NpgsqlDataReader npgSqlDataReader = npgSqlCommand.ExecuteReader();

                if (npgSqlDataReader.HasRows)                                                                                   //--если такой логин есть в бд
                {
                    foreach (DbDataRecord rec in npgSqlDataReader)
                    {
                        quest.Id = rec.GetInt32(0);
                    }
                }
                bd.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show("AddQuesAndReturnId ex:" + ex.Message);
            }
        }
        internal void UpdateQuestion(Question editQuest)
        {
            try
            {
                NpgsqlConnection bd = new NpgsqlConnection(connectionString);
                bd.Open();
                // update_quest(i_id integer, i_id_test integer, i_text character varying, i_number_quest integer, i_cost real)
                NpgsqlCommand npgSqlCommand = new NpgsqlCommand($"SELECT update_quest('{editQuest.Id}','{editQuest.IdTest}','{editQuest.Text}','{editQuest.Number}','{editQuest.Cost}');", bd);
                npgSqlCommand.ExecuteNonQuery();
                bd.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("UpdateQuestion ex:" + ex.Message);
            }
        }

        public void DeleteQuest(Question delQuestion)
        {
            try
            {
                NpgsqlConnection bd = new NpgsqlConnection(connectionString);
                bd.Open();

                NpgsqlCommand npgSqlCommand = new NpgsqlCommand($"SELECT delete_quest('{delQuestion.Id}');", bd);
                npgSqlCommand.ExecuteNonQuery();
                bd.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("DeleteQuest ex:" + ex.Message);
            }
        }
    }
}
