﻿namespace TestAccaunting
{
    public class User
    {
        //--Учетные данные преподавателя
        public int Id { get; set; }
        private string _login;
        public string Login
        {
            get { return _login; }
            set { _login = value; }
        }
        private string _password;
        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }
        private string _fio;
        public string FIO
        {
            get { return _fio; }
            set { _fio = value; }
        }

        public User(string login, string pass, string fio)
        {
            _login = Login;
            _password = pass;
            _fio = fio;
        }
        public User()
        {
           
        }
    }
}
