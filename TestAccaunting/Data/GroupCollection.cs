﻿using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.ComponentModel;
using TestAccaunting.DataBase;

namespace TestAccaunting.Data
{
    public class GroupCollection : INotifyPropertyChanged
    {
        private ObservableCollection<Group> _groupList;
        public ObservableCollection<Group> GroupList
        {
            get
            {
                return _groupList;
            }
            set
            {
                _groupList = value;
            }
        }

        public GroupCollection(ObservableCollection<Group> groupList)
        {
            _groupList = groupList;
        }
        public GroupCollection(DBManager dBManager)
        {
            _groupList = dBManager.GetAllGroup();
        }



       
        public ObservableCollection<Group> GetGroupCollection()
        {
            return _groupList;
        }
        public void Reset(DBManager dBManager)
        {
            dBManager.GetAllGroup(ref _groupList);
            //OnPropertyChanged("_groupList");
        }
        
        public void DeleteGroup(Group group)
        {
            App.DBManager.DeleteGroup(group.Id);
            _groupList.Remove(group);
        }

        internal void isChange(Group group)
        {
            foreach (var g in _groupList)
            {
                if (g == group)
                {
                    g.IsChanged = true;
                }
            }
        }

        internal void AllSave()
        {
            foreach (var v in _groupList)
            {
                if (v.IsChanged)
                {
                    if (v.Id != 0)
                        App.DBManager.UpdateGroup(v);
                    else
                        App.DBManager.AddGroup(v);
                }
            }
        }

        internal Group At(Group group)
        {
            foreach (var v in _groupList)
            {
                if (v == group)
                {
                    return v;
                }
            }
            return null;
        }

        internal IEnumerable GetGroupCollectionToString(string cathedra, string year)
        {
            ObservableCollection<string> observableCollection = new ObservableCollection<string>();
            //MessageBox.Show($"{cathedra}-{year}");
            if (!string.IsNullOrWhiteSpace(cathedra) && year != "(все)")
            {
                //--выборка по кафедре и году
                foreach (var v in _groupList)
                {
                    if (v.Cathedra == cathedra && v.Year.ToString() == year)
                        observableCollection.Add(v.Name);
                }
            }
            else if (!string.IsNullOrWhiteSpace(cathedra))
            {
                //--выборка по кафедре
                foreach (var v in _groupList)
                {
                    if (v.Cathedra == cathedra)
                        observableCollection.Add(v.Name);
                }
            }
            else if (year != "(все)" && !string.IsNullOrWhiteSpace(year))
            {
                //--выборка по году
                foreach (var v in _groupList)
                {
                    if (v.Year == Int32.Parse(year))
                        observableCollection.Add(v.Name);
                }
            }
            else
            {
                foreach (var v in _groupList)
                {
                    observableCollection.Add(v.Name);
                }
            }

                return observableCollection;
        }

        internal IEnumerable GetGroupCollectionToString()
        {
            ObservableCollection<string> grCollection = new ObservableCollection<string>();

            foreach (var v in _groupList)
            {
                grCollection.Add(v.Name);
            }

            return grCollection;
        }
        internal int GetId(string group)
        {
            foreach (var v in _groupList)
            {
                if (v.Name == group)
                    return v.Id;
            }
            return -1;
        }


        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string p_propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(p_propertyName));
            }
        }

        public bool CheckGroupReferences(Group delGroup)
        {
            return App.DBManager.CheckGroupReferences(delGroup.Id);
        }
    }
}
