﻿using System;
using System.ComponentModel;

namespace TestAccaunting.Data
{
    public class Group : INotifyPropertyChanged
    {
        private int _id;            //--id группы, нужно для запросов
        private string _cipher;     //--шифр группы
        private string _name;       //--наименование
        private string _cathedra;   //--кафедра
        private int _year;          //--номер курса
        private DateTime? _recDate;
        private bool _isChanged = true;

        public int Id
        {
            get
            {
                return _id;
            }
            set
            {

                if (value != _id)
                {
                    _id = value;
                    OnPropertyChanged("_id");
                }
            }
        }
        public string Cipher
        {
            get
            {
                return _cipher;
            }
            set
            {

                if (value != _cipher)
                {
                    _cipher = value;
                    OnPropertyChanged("_cipher");
                }
            }
        }
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {

                if (value != _name)
                {
                    _name = value;
                    OnPropertyChanged("_name");
                }
            }

        }
        public string Cathedra
        {
            get
            {
                return _cathedra;
            }
            set
            {

                if (value != _cathedra)
                {
                    _cathedra = value;
                    OnPropertyChanged("_cathedra");
                }
            }
        }
        public int Year
        {
            get
            {
                return _year;
            }
            set
            {

                if (value != _year)
                {
                    _year = value;
                    OnPropertyChanged("_year");
                }
            }
        }
        public DateTime? RecDate
        {
            get
            {
                return _recDate;
            }
            set
            {

                if (value != _recDate)
                {
                    _recDate = value;
                    OnPropertyChanged("_recDate");
                }
            }
        }
        public string RecDateString
        {
            get
            {
                string[] str = _recDate.ToString().Split(' ');
                return str[0];
            }
            set
            {
                DateTime dateTime = new DateTime();
                if (DateTime.TryParse(value, out dateTime))
                {
                    _recDate = dateTime;
                }
            }
        }
        public bool IsChanged
        {
            get
            {
                return _isChanged;
            }
            set
            {

                if (value != _isChanged)
                {
                    _isChanged = value;
                    OnPropertyChanged("_isChanged");
                }
            }
        }

        public Group(int id, string cipher, string name, string cathedra, int year, DateTime dateTime, bool isChanged)
        {
            _id = id;
            _cipher = cipher;
            _name = name;
            _cathedra = cathedra;
            _year = year;
            _recDate = dateTime;
            _isChanged = isChanged;

        }
        public Group()
        {
        }


        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string p_propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(p_propertyName));
            }
        }
    }
}
