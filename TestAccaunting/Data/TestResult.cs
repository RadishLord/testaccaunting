﻿using System.Windows.Media;

namespace TestAccaunting.Data
{
    public class TestResult
    {
        /* содержит результаты прохождений тестов*/
        public string Group { get; }

        public string Fio { get; }

        public string Test { get; }

        public int Total { get; }

        public int Right { get; }

        public int Wrong { get; }

        public int Miss { get; }

        public int Number { get; }

        public int Mark { get; }

        public System.Windows.Media.Brush GetColor
        {
            get
            {

                SolidColorBrush brush = new SolidColorBrush(Colors.Green);

                if (Mark > 5) brush.Color = Colors.Green;
                if (Mark == 4) brush.Color = Colors.YellowGreen;
                if (Mark == 3) brush.Color = Colors.Yellow;
                if (Mark < 2) brush.Color = Colors.Red;
                return brush; ;
            }
        }

        public TestResult(string group, string fio, string test, int total, int right, int wrong, int miss, int number, int mark)
        {
            Group = group ?? "unknown";
            Fio = fio ?? "unknown";
            Test = test ?? "unknown";
            Total = total;
            Right = right;
            Wrong = wrong;
            Miss = miss;
            Number = number;
            Mark = mark;
        }


    }
}