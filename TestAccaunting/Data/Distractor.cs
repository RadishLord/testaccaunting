﻿namespace TestAccaunting.Data
{
    public class Distractor
    {
        public int Id { get; set; }
        public int IdQuest { get; set; }
        public int Number { get; set; }
        public string Text { get; set; }
        public bool IsRight { get; set; }

        public Distractor(int id, int number, string text, bool isRight)
        {
            Id = id;
            Number = number;
            Text = text;
            IsRight = isRight;
        }

        public Distractor()
        {
        }

        public string IsRightString
        {
            get
            {
                return IsRight ? "Тип: верный ответ" : "Тип: дистрактор";
            }

        }

    }
}
