﻿using System;

namespace TestAccaunting.Data
{
    public class Test
    {


        public int Id { get; set; }
        public int IdTeacher { get; set; }
        public string Name { get; set; }
        public int CountQuestions { get; set; }
        public TimeSpan Interval { get; set; }
        public QuestionCollection QuestionCollection { get; set; }



        public Test(int id, int idTeacher, string name, int countQuestions)
        {
            Id = id;
            IdTeacher = idTeacher;
            Name = name;
            CountQuestions = countQuestions;
            QuestionCollection = new QuestionCollection(id);
            //Interval = interval;
        }

        public Test(int id)
        {
            QuestionCollection = new QuestionCollection(id);
        }

    }

}
