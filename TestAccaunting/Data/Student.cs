﻿namespace TestAccaunting.Data
{
    public class Student
    {
        public int Id { get; set; }
        public int IdGroup { get; set; }
        public int Year { get; set; }
        public string Fio { get; set; }
        public string Passw { get; set; }
        public string Login { get; set; }
        public string Group { get; set; }
        public string Cathedra { get; set; }
        public bool IsChanged { get; set; }


        public Student(int id, int idGroup, int year, string fio, string passw, string login, string group, string cathedra, bool isChaged)
        {
            Id = id;
            IdGroup = idGroup;
            Year = year;
            Fio = fio;
            Passw = passw;
            Login = login;
            Group = group;
            Cathedra = cathedra;
            IsChanged = isChaged;
        }
        public Student( string fio, string passw, string login, string group)
        {
            Fio = fio;
            Passw = passw;
            Login = login;
            Group = group;
        }

    }
}
