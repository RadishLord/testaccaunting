﻿using System.Collections.ObjectModel;
using System.ComponentModel;

namespace TestAccaunting.Data
{
    public class QuestionCollection : INotifyPropertyChanged
    {
        private int _id { get; set; }
        private ObservableCollection<Question> _questions;
        public ObservableCollection<Question> Question
        {
            get
            {
                if (_questions == null)
                {
                    LoadQuestions();
                }
                return _questions;
            }
            set => throw new System.NotImplementedException();
        }

        public QuestionCollection(int id)
        {
            _id = id;
            LoadQuestions();
        }


        
        private void LoadQuestions()
        {
            _questions = new ObservableCollection<Question>();
            _questions = App.DBManager.LoadQuestion(_id);
            OnPropertyChanged("_questions");
        }

        public void UpdateQuest()
        {
            OnPropertyChanged("_questions");
        }
        public void DeleteQuest(Question delQuestion)
        {
            App.DBManager.DeleteQuest(delQuestion);
            for (var i = 0; i < _questions.Count; i++)
            {
                if (_questions[i].Id == delQuestion.Id)
                {
                    _questions.RemoveAt(i);
                    break;
                }

            }

            _questions.Remove(delQuestion);
            OnPropertyChanged("_questions");
        }
        public Question At(int currentQuestId)
        {
            foreach (Question v in _questions)
            {
                if (v.Id == currentQuestId)
                    return v;
            }

            return null;
        }
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string pPropertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(pPropertyName));
            }
        }



    }
}