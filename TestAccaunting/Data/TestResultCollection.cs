﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using TestAccaunting.Annotations;
using TestAccaunting.DataBase;

namespace TestAccaunting.Data
{
    public class TestResultCollection : INotifyPropertyChanged
    {
        private ObservableCollection<TestResult> _testResultList = new ObservableCollection<TestResult>();
  

        public TestResultCollection(DBManager dBManager)
        {
            _testResultList = dBManager.GetTestResult();
        }

        public ObservableCollection<TestResult> TestResultList
        {
            get
            {
                return _testResultList;
            }
            set
            {
                _testResultList = value;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }



        public void GetTestResultByGroupAndTest(int group, int test, DBManager dBManager)
        {
            _testResultList.Clear();
            dBManager.GetTestResultByGroupAndTest(ref _testResultList, group, test);
        }

        public void GetTestResultByGroup(int group, DBManager dBManager)
        {
            _testResultList.Clear();
             dBManager.GetTestResultByGroup(ref _testResultList, group);
        }

        public void GetTestResultByTest(int test, DBManager dBManager)
        {
            _testResultList.Clear();
            dBManager.GetTestResultByTest(ref _testResultList, test);
        }
    }
}