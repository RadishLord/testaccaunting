﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using TestAccaunting.DataBase;

namespace TestAccaunting.Data
{
    class TestsCollection : INotifyPropertyChanged
    { 
        private ObservableCollection<Test> _testCollection = new ObservableCollection<Test>();
        public ObservableCollection<Test> TestCollection
        {
            get
            {
                return _testCollection;
            }
            set
            {
                _testCollection = value;
            }
        }



        public TestsCollection(ObservableCollection<Test> testCollection)
        {
            _testCollection = testCollection;
        }
        public TestsCollection(DBManager dBManager, User user)
        {
            _testCollection = dBManager.GetTestsList(user.Id);
        }

        public ObservableCollection<Test> GetTestCollection()
        {
            return _testCollection;
        }
        internal ObservableCollection<string> GetStudentCollectionToString()
        {
            ObservableCollection<string> observableCollection = new ObservableCollection<string>();
            foreach (var v in _testCollection)
            {
                observableCollection.Add(v.Name);
            }
            return observableCollection;
        }

        public void Reset(DBManager dBManager, User user)
        {
            _testCollection = dBManager.GetTestsList(user.Id);
            OnPropertyChanged("_testCollection");
        }

        internal void DeleteTest(string deleteTestName)
        {
            foreach (var v in _testCollection)
            {
                if (v.Name == deleteTestName)
                {
                    App.DBManager.DeleteTest(v.Id);
                    _testCollection.Remove(v);
                    return;
                }

            }
        }
        public void DeleteTest(Test test)
        {
            App.DBManager.DeleteTest(test.Id);
            _testCollection.Remove(test);
        }
        internal void AddTest(Test test)
        {
            App.DBManager.AddTest(test);
            _testCollection.Add(test);

        }
        internal void UpdateTest(Test test)
        {
            App.DBManager.UpdateTest(test);
            for (int i = 0; i < _testCollection.Count; i++)
            {
                if (_testCollection[i].Id == test.Id)
                {
                    _testCollection[i] = test;
                    break;
                }
                    
            }

        }
        internal Test At(Test test)
        {
            foreach (var s in _testCollection)
            {
                if (s == test)
                {
                    return s;
                }
            }
            return null;
        }
        internal Test At(string testName)
        {
            foreach (var v in _testCollection)
            {
                if (v.Name == testName)
                {
                    return v;
                }

            }
            return null;
        }
        internal Test At(int id)
        {
            foreach (var v in _testCollection)
            {
                if (v.Id == id)
                {
                    return v;
                }

            }
            return null;
        }
        

        public void UpdateQuest(Question editQuestion)
        {
            //--Находим нужный тест     
            //Test test = this.At(editQuestion.IdTest);
            ////--Находим нужный вопрос
            //for (int i = 0; i < test.Question.Count; i++)
            //{
            //    if (editQuestion.Id == test.Question[i].Id)
            //        test.Question[i] = editQuestion;
            //}

            //--меняем
            //--обновляем 
            App.DBManager.UpdateQuestion(editQuestion);
            this.At(editQuestion.IdTest).QuestionCollection.UpdateQuest();
            OnPropertyChanged("_testCollection");
            
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string pPropertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(pPropertyName));
            }
        }

        public int GetId(string test)
        {
            foreach (var v in _testCollection)
            {
                if (v.Name == test)
                    return v.Id;
            }

            return -1;
        }
    }
}