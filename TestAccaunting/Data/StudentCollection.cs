﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using TestAccaunting.DataBase;

namespace TestAccaunting.Data
{
    class StudentCollection : INotifyPropertyChanged
    {
        private ObservableCollection<Student> _studentList = new ObservableCollection<Student>();
        public ObservableCollection<Student> StudentList
        {
            get
            {
                return _studentList;
            }
            set
            {
                _studentList = value;
            }
        }

        public StudentCollection(ObservableCollection<Student> studentList)
        {
            _studentList = studentList;
        }
        public StudentCollection(DBManager dBManager)
        {
            _studentList = dBManager.GetAllStudents();
        }
        public void Reset(DBManager dBManager)
        {
            dBManager.GetAllStudents(ref _studentList);
            OnPropertyChanged("_studentList");
        }
        public ObservableCollection<Student> GetStudentCollection()
        {
            return _studentList;
        }


    

        public void DeleteStudent(Student student)
        {
            App.DBManager.DeleteStudent(student.Id);
            _studentList.Remove(student);
        }

        internal void isChange(Student student)
        {
            foreach (var s in _studentList)
            {
                if (s == student)
                {
                    s.IsChanged = true;
                }
            }
        }

        internal void AllSave()
        {
            foreach (var v in _studentList)
            {
                if (v.IsChanged)
                {
                    if (v.Id != 0)
                        App.DBManager.UpdateStudent(v);
                    else
                        App.DBManager.AddStudent(v);
                }
            }
        }

        internal void AddStudent(Student student)
        {
            App.DBManager.AddStudent(student);
            _studentList.Add(student);

        }

        internal Student At(Student student)
        {
            foreach (var s in _studentList)
            {
                if (s == student)
                {
                    return s;
                }
            }
            return null;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string p_propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(p_propertyName));
            }
        }

        internal void GetWithParam(string cathedra, string group, string year)
        {
            if (!string.IsNullOrWhiteSpace(group))
            {
                //--ищем по группе
                App.DBManager.GetStudentByGroup(ref _studentList, group);
            }
            else if (!string.IsNullOrWhiteSpace(cathedra) && !string.IsNullOrWhiteSpace(year))
            {
                //--ищем по кафедре и году
                App.DBManager.GetStudentByCathedraAndYear(ref _studentList, cathedra, year);
            }
            else if (!string.IsNullOrWhiteSpace(cathedra))
            {
                //--ищем по кафедре 
                App.DBManager.GetStudentByCathedra(ref _studentList, cathedra);
            }
            else if (!string.IsNullOrWhiteSpace(year))
            {
                //--ищем по году 
                App.DBManager.GetStudentByYear(ref _studentList, year);
            }
        }

        public bool CheckReferences(Student deleteStudent)
        {
            return App.DBManager.CheckReferences(deleteStudent.Id);
        }
    }
}

