﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;


namespace TestAccaunting.Data
{
    public class Question : INotifyPropertyChanged
    {
        public Question(int id, int number, int idTest, float cost, string text, int countDistractors)
        {
            Id = id;
            Number = number;
            IdTest = idTest;
            Cost = cost;
            Text = text ?? throw new ArgumentNullException(nameof(text));
            CountDistractors = countDistractors;
        }
        public Question()
        {

        }


        private int _id;
        private int _number;
        private int _idTest;
        private float _cost;
        private string _text;
        private int _countDistractors;
        private ObservableCollection<Distractor> _distractors;

        public int Id
         {
            get
            {

                return _id;
            }
            set
            {

                if (value != _id)
                {
                    _id = value;
                    OnPropertyChanged("_id");
                }
            }
        }
        public int Number
        {
            get
            {
                return _number;
            }
            set
            {

                if (value != _number)
                {
                    _number = value;
                    OnPropertyChanged("_number");
                }
            }
        }
        public int IdTest
        {
            get
            {
                return _idTest;
            }
            set
            {

                if (value != _idTest)
                {
                    _idTest = value;
                    OnPropertyChanged("_idTest");
                }
            }
        }
        public float Cost
        {
            get
            {
                return _cost;
            }
            set
            {

                if (value != _cost)
                {
                    _cost = value;
                    OnPropertyChanged("_cost");
                }
            }
        }
        public string Text
        {
            get
            {
                return _text;
            }
            set
            {

                if (value != _text)
                {
                    _text = value;
                    OnPropertyChanged("_text");
                }
            }
        }      
        public int CountDistractors
        {
            get
            {
                return _countDistractors;
            }
            set
            {

                if (value != _countDistractors)
                {
                    _countDistractors = value;
                    OnPropertyChanged("CountDistractors");
                }
            }
        }
        public ObservableCollection<Distractor> Distractors
        {
            get
            {
                if (_distractors == null)
                    LoadDistractors();
                return _distractors;
            }
        }




        internal void LoadDistractors()
        {
            _distractors = App.DBManager.LoadDistractors(Id);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string pPropertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(pPropertyName));
            }
        }


        public void AddDistr(Distractor newDistractor)
        {
            App.DBManager.AddDistrAndReturnId(ref newDistractor);
            _distractors.Add(newDistractor);
            _countDistractors++;
        }

        public void DeleteDistr(Distractor deleteDistractor)
        {
            App.DBManager.DeleteDistractor(deleteDistractor);
            _distractors.Remove(deleteDistractor);
            _countDistractors--;
        }

        public void UpdateDistr(Distractor editDistractor)
        {
            App.DBManager.UpdateDistractor(editDistractor);
            for (int i = 0; i < _distractors.Count; i++)
            {
                if (_distractors[i].Id == editDistractor.Id)
                {
                    _distractors[i] = editDistractor;
                }
            }
        }
    }
}
