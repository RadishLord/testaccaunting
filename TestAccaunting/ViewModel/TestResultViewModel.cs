﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using TestAccaunting.Data;

namespace TestAccaunting.ViewModel
{
    public class TestResultViewModel : DependencyObject
    {

        public ICollectionView Items
        {
            get { return (ICollectionView)GetValue(ItemsProperty); }
            set { SetValue(ItemsProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MyProperty.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ItemsProperty =
            DependencyProperty.Register("Items", typeof(ICollectionView), typeof(TestResultViewModel), new PropertyMetadata(null));

        public TestResultViewModel(TestResultCollection testResultCollection)
        {
            Items = System.Windows.Data.CollectionViewSource.GetDefaultView(testResultCollection.TestResultList);
        }

    }
}