﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using TestAccaunting.Data;

namespace TestAccaunting.ViewModel
{
    class StudentViewModel : DependencyObject
    {

        public string FilterText
        {
            get { return (string)GetValue(FilterTextProperty); }
            set { SetValue(FilterTextProperty, value); }
        }

        // Using a DependencyProperty as the backing store for FilterText.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty FilterTextProperty =
            DependencyProperty.Register("FilterText", typeof(string), typeof(StudentViewModel), new PropertyMetadata("", FilterText_Changed));

        private static void FilterText_Changed(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var current = d as StudentViewModel;
            if (current != null)
            {
                current.Items.Filter = null;
                current.Items.Filter = current.FilterStudent;
            }
        }

        public ICollectionView Items
        {
            get { return (ICollectionView)GetValue(ItemsProperty); }
            set { SetValue(ItemsProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MyProperty.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ItemsProperty =
            DependencyProperty.Register("Items", typeof(ICollectionView), typeof(StudentViewModel), new PropertyMetadata(null));

        public StudentViewModel(StudentCollection studentCollection)
        {

            Items = System.Windows.Data.CollectionViewSource.GetDefaultView(studentCollection.GetStudentCollection());
            Items.Filter = FilterStudent;
        }

        private bool FilterStudent(object obj)
        {
            bool result = true;
            Student current = obj as Student;


            if (!string.IsNullOrWhiteSpace(FilterText)
                && current != null
                && current.Fio.IndexOf(FilterText, StringComparison.OrdinalIgnoreCase) == -1
                && current.Cathedra.IndexOf(FilterText, StringComparison.OrdinalIgnoreCase) == -1
                && current.Group.IndexOf(FilterText, StringComparison.OrdinalIgnoreCase) == -1)
            {
                result = false;
            }

            int year;
            if (int.TryParse(FilterText, out year))
            {
                if (current.Year != year)
                    result = false;
            }

            return result;
        }

        public void Refresh(StudentCollection studentCollection)
        {
            Items = System.Windows.Data.CollectionViewSource.GetDefaultView(studentCollection.GetStudentCollection());
        }
    }

}
