﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using TestAccaunting.Data;

namespace TestAccaunting.ViewModel
{
    class TestViewModel : DependencyObject
    {
          
        public ICollectionView Items
        {
            get { return (ICollectionView)GetValue(ItemsProperty); }
            set { SetValue(ItemsProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MyProperty.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ItemsProperty =
            DependencyProperty.Register("Items", typeof(ICollectionView), typeof(TestViewModel), new PropertyMetadata(null));

        public TestViewModel(Test test)
        {
            ObservableCollection<Question> questList = test.QuestionCollection.Question;
            Items = System.Windows.Data.CollectionViewSource.GetDefaultView(questList);
        }


        public void Refresh(Test test)
        {
            ObservableCollection<Question> questList = test.QuestionCollection.Question;
            Items = System.Windows.Data.CollectionViewSource.GetDefaultView(questList);
        }
    }
}
