﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using TestAccaunting.Data;
using TestAccaunting.DataBase;

namespace TestAccaunting.ViewModel
{
    class GroupViewModel : DependencyObject
    {
        
        public string FilterText
        {
            get { return (string)GetValue(FilterTextProperty); }
            set { SetValue(FilterTextProperty, value); }
        }

        // Using a DependencyProperty as the backing store for FilterText.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty FilterTextProperty =
            DependencyProperty.Register("FilterText", typeof(string), typeof(GroupViewModel), new PropertyMetadata("", FilterText_Changed));

        private static void FilterText_Changed(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var current = d as GroupViewModel;
            if (current != null)
            {
                current.Items.Filter = null;
                current.Items.Filter = current.FilterGroup;
            }
        }

        public ICollectionView Items
        {
            get { return (ICollectionView)GetValue(ItemsProperty); }
            set { SetValue(ItemsProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MyProperty.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ItemsProperty =
            DependencyProperty.Register("Items", typeof(ICollectionView), typeof(GroupViewModel), new PropertyMetadata(null));

        public GroupViewModel(GroupCollection groupCollection)
        {

            Items = CollectionViewSource.GetDefaultView(groupCollection.GetGroupCollection());
            Items.Filter = FilterGroup;
        }

        private bool FilterGroup(object obj)
        {
            bool result = true;
            Group current = obj as Group;


            if (!string.IsNullOrWhiteSpace(FilterText)
                && current != null 
                && current.Name.IndexOf(FilterText, StringComparison.OrdinalIgnoreCase) == -1 
                && current.Cathedra.IndexOf(FilterText, StringComparison.OrdinalIgnoreCase) == -1
                && current.RecDateString.IndexOf(FilterText, StringComparison.OrdinalIgnoreCase) == -1)
            {
                result = false;
            }

            int year;
            if (int.TryParse(FilterText, out year))
            {
                if (current.Year != year)
                    result = false;
            }

            return result;
        }

        public void Refresh(GroupCollection groupCollection)
        {
            Items = CollectionViewSource.GetDefaultView(groupCollection.GetGroupCollection());
        }
    }
}
