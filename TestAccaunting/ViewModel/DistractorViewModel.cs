﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using TestAccaunting.Data;

namespace TestAccaunting.ViewModel
{
    class DistractorViewModel : DependencyObject
    {
        

        public ICollectionView Items
        {
            get { return (ICollectionView)GetValue(ItemsProperty); }
            set { SetValue(ItemsProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MyProperty.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ItemsProperty =
            DependencyProperty.Register("Items", typeof(ICollectionView), typeof(DistractorViewModel), new PropertyMetadata(null));

        public DistractorViewModel(Question quustion)
        {

            Items = System.Windows.Data.CollectionViewSource.GetDefaultView(quustion.Distractors);
        }
    }
}
