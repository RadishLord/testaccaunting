﻿CREATE OR REPLACE FUNCTION update_test(i_id int, i_id_teacher integer, i_name character varying, i_count_quest integer)
  RETURNS void AS
$BODY$
BEGIN

	UPDATE test SET 
	id_teacher = i_id_teacher,
	name_test = i_name,
	question_count = i_count_quest
	WHERE id_test = i_id;
	
END;
$BODY$
  LANGUAGE plpgsql;
