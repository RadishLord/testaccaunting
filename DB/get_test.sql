﻿CREATE OR REPLACE FUNCTION get_tests( IN i_id_teacher int,
OUT o_id_test integer, 
OUT o_name character varying, 
OUT o_question_count integer
)
  RETURNS SETOF record AS
$BODY$
begin

	
	FOR o_id_test, o_name, o_question_count IN SELECT id_test, name_test, question_count FROM test WHERE id_teacher =  i_id_teacher LOOP
		RETURN next;
	END LOOP;
	
	RETURN;


END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION get_all_group()
  OWNER TO postgres;

SELECT * FROM get_tests('8');