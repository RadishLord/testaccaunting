﻿--триггерная функция на вставку в study_group проверяющая (имя + год)

CREATE OR REPLACE FUNCTION trig_func_study_group_unique_check()
RETURNS trigger AS
$BODY$
BEGIN
	IF NOT EXISTS(SELECT * FROM study_group WHERE recruitment_year = NEW.recruitment_year 
		AND name_group = NEW.name_group) THEN
		RAISE NOTICE 'Триггер trig_func_study_group_unique_check - ОК ';
		return NEW;
	END IF;
	RAISE NOTICE 'Триггер trig_func_study_group_unique_check - ERROR ';
	return OLD;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;


--тригер 
CREATE TRIGGER triger_study_group_unique
BEFORE INSERT OR UPDATE ON study_group
FOR EACH ROW
EXECUTE PROCEDURE trig_func_study_group_unique_check();
