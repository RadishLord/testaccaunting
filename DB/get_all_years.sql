﻿--Высчитывает курсы по годам
CREATE OR REPLACE FUNCTION get_number_cource(i_id int)
RETURNS int AS
$BODY$
DECLARE
_date date;
_dif int;
r real;
begin

	SELECT INTO _date DISTINCT recruitment_year FROM study_group WHERE id_group = i_id;
	RAISE NOTICE'= %',_date;
	_dif := current_date - _date;
	r := (_dif / 356) + 1;
	RAISE NOTICE'- %',_dif;
	RAISE NOTICE'/ %',r;
	return r;
	
END;
$BODY$
LANGUAGE plpgsql;

SELECT get_number_cource(5);