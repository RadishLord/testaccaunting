﻿-- Function: get_all_group()

-- DROP FUNCTION get_all_group();

CREATE OR REPLACE FUNCTION get_all_group(
OUT o_id integer, 
OUT o_name character varying,
OUT o_cathedra character varying,
OUT o_cource int)
  RETURNS SETOF record AS
$BODY$
begin

	
	FOR o_id, o_name,o_cathedra IN SELECT id_group, name_group, cathedra, recruitment_year FROM study_group ORDER BY recruitment_year DESC LOOP
		SELECT INTO o_cource get_number_cource(o_id);
		RETURN next;
	END LOOP;
	
	RETURN;


END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION get_all_group()
  OWNER TO postgres;

SELECT * FROM get_all_group();