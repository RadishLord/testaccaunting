﻿CREATE OR REPLACE FUNCTION generate_login(fio character varying)
  RETURNS varchar AS
$BODY$
DECLARE
result varchar;
arr varchar[];
chars text[] := '{0,1,2,3,4,5,6,7,8,9}';
begin

	arr := regexp_split_to_array(lower(fio), E'\\s+');

	result := left(arr[1], 2) || left(arr[2], 2) || left(arr[3], 2);
	
	WHILE EXISTS (SELECT login FROM users WHERE result = login) LOOP
		result := result || chars[1+random()*(array_length(chars, 1)-1)];
	END LOOP;

	return result; 
END;
$BODY$
LANGUAGE plpgsql VOLATILE;


SELECT generate_login('Орехов Леонид Иванович');