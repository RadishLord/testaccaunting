﻿-- Function: login_check(character varying)

-- DROP FUNCTION login_check(character varying);

CREATE OR REPLACE FUNCTION check_group_references(i_id_group integer)
  RETURNS boolean AS
$BODY$
BEGIN

	RETURN EXISTS(SELECT * FROM users WHERE id_group = i_id_group);

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION login_check(character varying)
  OWNER TO postgres;

  
SELECT check_group_references(38);