﻿CREATE OR REPLACE FUNCTION get_all_group(OUT o_id integer, OUT o_name character varying, OUT o_cathedra character varying, OUT o_cource integer, OUT o_rec_date date)
  RETURNS SETOF record AS
$BODY$
begin

	
	FOR o_id, o_name,o_cathedra,o_rec_date IN SELECT id_group, name_group, cathedra, recruitment_year FROM study_group ORDER BY recruitment_year DESC LOOP
		SELECT INTO o_cource get_number_cource(o_id);
		RETURN next;
	END LOOP;
	
	RETURN;


END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION get_all_group()
  OWNER TO postgres;
