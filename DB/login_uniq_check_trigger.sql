﻿--проверка паролей на длину 
DROP  FUNCTION IF EXISTS login_uniq_check_trigger_func() CASCADE;
CREATE FUNCTION login_uniq_check_trigger_func () RETURNS trigger AS $$
DECLARE
BEGIN

	IF (EXISTS (SELECT * FROM users WHERE login = new.login)) THEN
		RAISE NOTICE 'Логин существует';
		return old;
	END IF;
	
        RETURN new;
END;
$$ LANGUAGE plpgsql;


create trigger login_uniq_check_trigger
BEFORE  update or insert on users for row
execute procedure login_uniq_check_trigger_func();

