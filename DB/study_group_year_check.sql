﻿--триггерная функция на вставку в study_group проверяющая год

CREATE OR REPLACE FUNCTION trig_func_study_group_year_check()
RETURNS trigger AS
$BODY$
BEGIN
	
	IF (NEW.recruitment_year <= now()) THEN
		RAISE NOTICE 'Триггер trig_func_study_group_year_checк - ОК ';
		return NEW;
	END IF;
	RAISE NOTICE 'Триггер trig_func_study_group_year_checк - ERROR ';
	return OLD;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;


--тригер к заданию 9.1
CREATE TRIGGER triger_study_group
BEFORE INSERT OR UPDATE ON study_group
FOR EACH ROW
EXECUTE PROCEDURE trig_func_study_group_year_check();
