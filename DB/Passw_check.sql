﻿CREATE OR REPLACE FUNCTION teacher_registration(i_login character varying, 
						i_passw character varying, 
						i_fio character varying)
RETURNS void AS
$BODY$
BEGIN
	
	INSERT INTO teacher (fio,password,login) VALUES
	(i_fio, i_passw, i_login);

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION login_check(character varying)
  OWNER TO postgres;
  
SELECT teacher_registration('coval','qqaaqq','Коваленко В.Н.');