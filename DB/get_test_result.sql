﻿CREATE OR REPLACE FUNCTION get_test_result(
OUT o_group character varying, 
OUT o_fio character varying,
OUT o_test character varying,
OUT o_total integer, 
OUT o_right integer, 
OUT o_wrong integer, 
OUT o_missed integer,
OUT o_attempt integer,
OUT o_mark integer)
RETURNS SETOF record AS
$BODY$
DECLARE
p_user_id integer;
p_test_id integer;
p_id_group integer;
begin




	FOR p_user_id, p_test_id, o_total, o_right, o_wrong, o_missed, o_attempt, o_mark	 
	IN SELECT r.id_user, r.id_test,  r.total_quest, r.right_quest,r.wrong_quest, r.missed_quest, 	r.attempt_number, r.mark 
	FROM test_result r  LOOP

		--вытаскиваем фамилию в вывод
		SELECT INTO o_fio fio FROM users WHERE id_user = p_user_id;
		--вытаскиваем айди группы по юзеру
		SELECT INTO p_id_group id_group FROM users WHERE id_user = p_user_id;
		--вытаскиваем название группы по айди
		SELECT INTO o_group name_group FROM study_group WHERE id_group = p_id_group;
		--вытаскиваем название теста
		SELECT INTO o_test name_test FROM test WHERE id_test = p_test_id;
		RETURN next;
		
	END LOOP;
	
	RETURN;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;



SELECT * FROM get_test_result();