﻿CREATE OR REPLACE FUNCTION delete_student(i_id integer)
  RETURNS void AS
$BODY$
BEGIN
	DELETE  FROM users
	WHERE id_user = i_id;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;
