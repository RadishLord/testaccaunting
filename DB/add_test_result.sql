﻿CREATE OR REPLACE FUNCTION add_test_result(int,int,int,int,int,int,interval,int,int,int,timestamp with time zone)
  RETURNS void AS
$BODY$
BEGIN
	
	INSERT INTO test_result (id_user, id_test, total_quest, wrong_quest,right_quest,missed_quest,time_passed,mark,percent,attempt_number,"time" ) VALUES
	($1, $2, $3, $4,$5,$6,$7,$8,$9,$10,$11);

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;


SELECT add_test_result(83,12,10,6,1,3,'00:20:21',2,10,1,'2017-10-12 09:30:00+04');