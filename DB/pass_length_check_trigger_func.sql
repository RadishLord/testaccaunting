﻿--проверка паролей на длину 
DROP  FUNCTION IF EXISTS pass_length_check_trigger_func() CASCADE;
CREATE FUNCTION pass_length_check_trigger_func () RETURNS trigger AS $$
DECLARE
BEGIN

	IF (length(new.passw) < 5) THEN
		RAISE NOTICE 'Ненадежный пароль';
		return old;
	END IF;
	
        RETURN new;
END;
$$ LANGUAGE plpgsql;


create trigger pass_length_check_trigger
BEFORE  update or insert on users for row
execute procedure pass_length_check_trigger_func();

