﻿CREATE OR REPLACE FUNCTION get_questions(IN i_id_test int, 
OUT o_id integer, 
OUT o_number integer, 
OUT o_cost real,
OUT o_text varchar, 
OUT o_count_distr integer)
 
  RETURNS SETOF record AS
$BODY$
begin

	
	FOR o_id, o_number,o_cost,o_text,o_count_distr  IN SELECT id_quest, number_quest, cost_quest, text_quest,count_distractor  FROM question WHERE id_test = i_id_test LOOP
		RETURN next;
	END LOOP;
	
	RETURN;


END;
$BODY$
  LANGUAGE plpgsql;

SELECT * FROM get_questions('1');