﻿--ваствка оценок в marks
CREATE OR REPLACE FUNCTION marks_insert(p_mark int, p_percent int, p_id_quest int)
RETURNS void AS
$BODY$
BEGIN

	INSERT INTO marks (mark, percent, id_quest) VALUES
	(p_mark, p_percent, p_id_quest);

END;
$BODY$
LANGUAGE plpgsql;

SELECT marks_insert('5','90','1');