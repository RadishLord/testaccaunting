﻿-- Function: add_distr(integer, character varying, boolean, integer)

-- DROP FUNCTION add_distr(integer, character varying, boolean, integer);

CREATE OR REPLACE FUNCTION add_distr(i_number integer, i_text character varying, i_is_right boolean, i_id_quest integer)
  RETURNS void AS
$BODY$
BEGIN
	
	INSERT INTO distractor (number_distractor, text_distractor, is_right, id_quest) VALUES
	(i_number, i_text, i_is_right, i_id_quest);

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
