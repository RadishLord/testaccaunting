﻿--Функция удаления группы

CREATE OR REPLACE FUNCTION study_group_delete(p_id int)
RETURNS void AS
$BODY$
BEGIN
	DELETE  FROM study_group
	WHERE id_group = p_id;
END;
$BODY$
LANGUAGE plpgsql;

