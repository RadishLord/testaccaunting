﻿--проверка паролей на длину 
DROP  FUNCTION IF EXISTS login_length_check_trigger_func() CASCADE;
CREATE FUNCTION login_length_check_trigger_func () RETURNS trigger AS $$
DECLARE
BEGIN

	IF (length(new.login) < 5) THEN
		RAISE NOTICE 'Слишком короткий логин';
		return old;
	END IF;
	
        RETURN new;
END;
$$ LANGUAGE plpgsql;


create trigger login_length_check_trigger
BEFORE  update or insert on users for row
execute procedure login_length_check_trigger_func();

