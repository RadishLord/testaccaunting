﻿CREATE OR REPLACE FUNCTION get_distractors(IN i_id_quest int, 
OUT o_id integer, 
OUT o_number integer, 
OUT o_text varchar, 
OUT o_flag boolean)
 
  RETURNS SETOF record AS
$BODY$
begin

	
	FOR o_id, o_number, o_text,o_flag  IN SELECT id_distractor, number_distractor, text_distractor, is_right  FROM distractor WHERE id_quest = i_id_quest LOOP
		RETURN next;
	END LOOP;
	
	RETURN;


END;
$BODY$
  LANGUAGE plpgsql;

SELECT * FROM get_distractors('1');