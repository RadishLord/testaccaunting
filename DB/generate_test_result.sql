﻿--Генерирует результаты тестов

CREATE OR REPLACE FUNCTION generate_test_result()
RETURNS void AS
$BODY$
declare
p_i integer := 1;
p_id_user integer := 0;
p_id_test integer := 0;
p_total integer;
p_wrong integer;
p_right integer;
p_missed integer;
p_percent integer;
p_mark integer := 2;
p_attempt integer = 1; 
p_date timestamp with time zone;
begin

	--идём по пользователям
	FOR p_id_user in SELECT id_user FROM users LOOP

		
		--идём по тестам
		FOR p_id_test IN SELECT id_test FROM test LOOP
			--определяем количество попыток
			SELECT INTO p_attempt floor(random()*(3-1+1))+1;

			FOR p_i in 1..p_attempt LOOP
			
				SELECT INTO p_total question_count FROM test WHERE id_test = p_id_test;
				SELECT INTO p_wrong floor(random()*(p_total-0+1))+0;
				SELECT INTO p_right floor(random()*((p_total-p_wrong)-0+1))+0;
				p_missed := p_total - (p_wrong+p_right);
				p_percent := 100 * p_right / 10;

				IF (p_percent > 50) THEN p_mark = 3; END IF;
				IF (p_percent > 70) THEN p_mark = 4; END IF;
				IF (p_percent > 90) THEN p_mark = 5; END IF;

				SELECT INTO p_date to_timestamp(1388534400+random()*63071999);
				
				raise notice '%, %, %, %, %, %, %, %, %, %, %',p_id_user, p_id_test, p_total, p_wrong, p_right, p_missed,'00:45:00',p_mark, p_percent ,p_i, p_date;
				--SELECT add_test_result(p_id_user, p_id_test, p_total, p_wrong, p_right, p_missed,'00:45:00',p_mark, p_percent ,p_i, p_date);

				INSERT INTO test_result (id_user, id_test, total_quest, wrong_quest,right_quest,missed_quest,time_passed,mark,percent,attempt_number,"time" ) VALUES
				(p_id_user, p_id_test, p_total, p_wrong, p_right, p_missed,'00:45:00',p_mark, p_percent ,p_i, p_date);

				
			END LOOP;		
		END LOOP;
		

	END LOOP;

end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;


SELECT generate_test_result();