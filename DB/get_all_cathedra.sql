﻿CREATE OR REPLACE FUNCTION get_all_cathedra()
  RETURNS table(o_cathedra varchar) AS
$BODY$
begin

	
	RETURN QUERY SELECT DISTINCT cathedra FROM study_group; 


END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION get_all_group()
  OWNER TO postgres;


SELECT get_all_cathedra();