﻿--Функция удаления студента

CREATE OR REPLACE FUNCTION student_delete(p_id int)
RETURNS void AS
$BODY$
BEGIN
	DELETE  FROM student
	WHERE id_student = p_id;
END;
$BODY$
LANGUAGE plpgsql;

SELECT student_delete('6');