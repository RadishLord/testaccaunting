﻿CREATE OR REPLACE FUNCTION get_student
(
character varying,
OUT o_id integer, 
OUT o_id_group integer, 
OUT o_course integer, 
OUT o_fio character varying, 
OUT o_passw character varying, 
OUT o_login character varying, 
OUT o_group character varying, 
OUT o_cathedra character varying 
)
RETURNS SETOF record AS
$BODY$
DECLARE
_query ALIAS FOR $1;
begin

	
	--FOR o_id, o_id_group, o_fio, o_login, o_passw IN SELECT id_user, id_group, fio, login, passw FROM users  LOOP
	FOR o_id, o_id_group, o_fio, o_login, o_passw IN EXECUTE _query LOOP
		SELECT INTO o_course get_number_cource(o_id_group);
		SELECT INTO o_group name_group FROM study_group WHERE o_id_group = id_group;
		SELECT INTO o_cathedra cathedra FROM study_group WHERE o_id_group = id_group;
		RETURN next;
	END LOOP;
	
	RETURN;


END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION get_all_group()
  OWNER TO postgres;

SELECT * FROM get_student('SELECT id_user, u.id_group, fio, login, passw FROM users u, study_group g WHERE u.id_group = g.id_group AND g.cathedra = 'ИИТИК'');