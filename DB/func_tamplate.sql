﻿CREATE OR REPLACE FUNCTION get_teacher_data(i_login varchar, i_password varchar)
RETURNS table(о_fio varchar) AS
$BODY$
BEGIN

	RETURN QUERY SELECT fio FROM teacher WHERE login = i_login AND password = i_password;

END;
$BODY$
LANGUAGE plpgsql;


SELECT get_teacher_data('daniel','qwerty');