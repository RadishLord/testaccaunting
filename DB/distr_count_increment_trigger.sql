﻿CREATE FUNCTION distr_count_increment () RETURNS trigger AS $$
BEGIN

	UPDATE question   
	SET count_distractor = count_distractor + 1 WHERE id_quest = new.id_quest;

        
        RETURN NEW;
END;
$$ LANGUAGE plpgsql;

drop trigger if exists distr_count_increment_trigger on distractor;
create trigger distr_count_increment_trigger
before insert on distractor for row
execute procedure distr_count_increment();