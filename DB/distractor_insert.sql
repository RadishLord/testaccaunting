﻿--ваствка дистрактора в distractor
CREATE OR REPLACE FUNCTION distractor_insert(p_number_distractor int, p_text_distractor varchar, p_is_right boolean)
RETURNS void AS
$BODY$
BEGIN

	INSERT INTO distractor (number_distractor, text_distractor, is_right) VALUES
	(p_number_distractor, p_text_distractor, p_is_right);

END;
$BODY$
LANGUAGE plpgsql;

SELECT distractor_insert('1','MJHFKFH','TRUE');