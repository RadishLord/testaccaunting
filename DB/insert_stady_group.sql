﻿--Функция вставки группы

CREATE OR REPLACE FUNCTION study_group_insert(p_name varchar, p_cathedra varchar, recruit_year date)
RETURNS void AS
$BODY$
BEGIN

	INSERT INTO study_group (name_group, cathedra, recruitment_year) VALUES
	(p_name, p_cathedra, recruit_year);

END;
$BODY$
LANGUAGE plpgsql;

SELECT study_group_insert('ДИПРб-12','ИИТИК','02-09-2017');