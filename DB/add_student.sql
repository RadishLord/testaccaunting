﻿-- Function: add_student(integer, character varying, character varying, character varying)

-- DROP FUNCTION add_student(integer, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION add_student(i_id_group integer, i_fio character varying, i_login character varying, i_pass character varying)
  RETURNS void AS
$BODY$
BEGIN

	IF (i_login = '') THEN
		i_login := generate_login(i_fio);
	END IF;

	IF (i_pass = '') THEN
		i_pass := generate_password('6');
	END IF;
	
	INSERT INTO users (id_group,fio ,login, passw) VALUES
	(i_id_group, i_fio, i_login, i_pass);

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION add_student(integer, character varying, character varying, character varying)
  OWNER TO postgres;
