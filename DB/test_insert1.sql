﻿--ваствка теста в test
CREATE OR REPLACE FUNCTION test_insert(p_id_teacher int, p_id_marks int, p_name_test varchar, p_question_count int, p_time interval)
RETURNS void AS
$BODY$
BEGIN

	INSERT INTO distractor (id_teacher, id_marks, name_test, question_count, time) VALUES
	(p_id_teacher, p_id_marks, p_name_test, p_question_count, p_time);

END;
$BODY$
LANGUAGE plpgsql;

SELECT test_insert('1','MJHFKFH','TRUE');