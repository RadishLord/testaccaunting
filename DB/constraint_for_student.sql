﻿ALTER TABLE  student
  ADD CONSTRAINT id_group_fkey2 FOREIGN KEY (id_group)
      REFERENCES study_group (id_group) MATCH FULL
      ON UPDATE NO ACTION ON DELETE CASCADE;