﻿CREATE OR REPLACE FUNCTION update_distr(i_id int, i_number int, i_text character varying, i_is_right boolean, i_id_quest int)
  RETURNS void AS
$BODY$
BEGIN

	UPDATE distractor SET 
	number_distractor = i_number,
	text_distractor = i_text,
	is_right = i_is_right,
	id_quest = i_id_quest
	WHERE id_distractor = i_id;
	
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
