﻿--таблица "учитель"
CREATE TABLE teacher
(
	id_teacher	serial NOT NULL, 
	fio		character varying(50) NOT NULL,	
	password	character varying(30) NOT NULL,	
	login 		character varying(30) NOT NULL,	

	CONSTRAINT id_teacher_pkey PRIMARY KEY (id_teacher)
);
--таблица "группа"
CREATE TABLE study_group
(
	id_group	serial NOT NULL,
	name_group	character varying(20) NOT NULL,
	cathedra	character varying(30) NOT NULL,
	recruitment_year	date,

	CONSTRAINT id_group_pkey PRIMARY KEY (id_group),
	CONSTRAINT name_group_unique UNIQUE (name_group)
);
--таблица "пользователь"
CREATE TABLE users
(
	id_user		serial NOT NULL, 
	id_group	int NOT NULL,
	fio		character varying(50) NOT NULL,
	login		character varying(50) NOT NULL,

	CONSTRAINT id_user_pkey PRIMARY KEY (id_user),
	CONSTRAINT "id_group_fkey" FOREIGN KEY (id_group)
		REFERENCES study_group (id_group) MATCH SIMPLE
);
--таблица "тест"
CREATE TABLE test
(
	id_test		serial NOT NULL, 
	id_teacher	int NOT NULL,
	name_test       character varying(50) NOT NULL,
	question_count	int,	
	time		interval,


	CONSTRAINT id_test_pkey PRIMARY KEY (id_test),
	CONSTRAINT "id_teacher_fkey" FOREIGN KEY (id_teacher)
		REFERENCES teacher (id_teacher) MATCH SIMPLE
);
--таблица "дистрактор"
CREATE TABLE distractor
(
	id_distractor		serial NOT NULL,
	number_distractor	int NOT NULL,
	text_distractor		character varying(100) NOT NULL,
	is_right		boolean NOT NULL,

	CONSTRAINT id_distractort_pkey PRIMARY KEY (id_distractor)
);
--таблица "вопрос"
CREATE TABLE question
(
	id_quest		serial NOT NULL,	
	id_test			int NOT NULL,
	cost_quest		int,	
	text_quest		character varying(150) NOT NULL,	
	id_distractor		int NOT NULL,	
	number_quest		int NOT NULL,	
	distractor_count	int NOT NULL	,


	CONSTRAINT id_quest_pkey PRIMARY KEY (id_quest),
	CONSTRAINT "id_distractor_fkey" FOREIGN KEY (id_distractor)
		REFERENCES distractor (id_distractor) MATCH SIMPLE
);
--таблица "результаты тестирования"
CREATE TABLE test_resoults
(
	id_test_resoults        serial NOT NULL,
	id_user			int NOT NULL,
	id_test			int NOT NULL,
	total_quest		int NOT NULL,
	wrong_quest		int NOT NULL,
	right_quest		int NOT NULL,
	missed_quest		int NOT NULL,
	time_passed		interval,
	mark			int NOT NULL,
	percent			int NOT NULL,
	attempt_number		int NOT NULL,
	time			timestamp with time zone,


	CONSTRAINT id_test_resoults_pkey PRIMARY KEY (id_test_resoults),
	CONSTRAINT "id_student_fkey" FOREIGN KEY (id_user)
		REFERENCES users (id_user) MATCH SIMPLE,
	CONSTRAINT "id_test_fkey" FOREIGN KEY (id_test)
		REFERENCES test (id_test) MATCH SIMPLE
);
