﻿-- Function: add_distr(integer, character varying, boolean, integer)

-- DROP FUNCTION add_distr(integer, character varying, boolean, integer);

CREATE OR REPLACE FUNCTION add_distr_and_return_id(i_number integer, i_text character varying, i_is_right boolean, i_id_quest integer)
  RETURNS integer AS
$BODY$
BEGIN
	
	INSERT INTO distractor (number_distractor, text_distractor, is_right, id_quest) VALUES
	(i_number, i_text, i_is_right, i_id_quest);

	RETURN  id_distractor FROM distractor WHERE number_distractor = i_number AND 
							text_distractor = i_text AND 
							is_right = i_is_right AND 
							id_quest = i_id_quest;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
