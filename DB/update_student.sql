﻿CREATE OR REPLACE FUNCTION update_student(i_id integer, i_id_group integer, i_fio character varying,  i_login character varying, i_pass character varying)
  RETURNS void AS
$BODY$
BEGIN

	UPDATE users SET 
	id_group = i_id_group,
	fio = i_fio,
	login = i_login,
	passw = i_pass
	WHERE id_user = i_id;
	
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;