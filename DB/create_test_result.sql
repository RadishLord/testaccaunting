﻿CREATE TABLE test_result
(
  id_test_result serial NOT NULL,
  id_user integer NOT NULL,
  id_test integer NOT NULL,
  id_distractor integer NOT NULL,

  CONSTRAINT id_test_result_pkey PRIMARY KEY (id_test_result),
  CONSTRAINT id_student_fkey FOREIGN KEY (id_user)
      REFERENCES users (id_user) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT id_test_fkey FOREIGN KEY (id_test)
      REFERENCES test (id_test) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
  CONSTRAINT id_distractor_fkey FOREIGN KEY (id_distractor)
      REFERENCES distractor (id_distractor) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE test_resoults
  OWNER TO postgres;
