﻿CREATE OR REPLACE FUNCTION login_check(i_login character varying)
  RETURNS boolean AS
$BODY$
BEGIN

	RETURN EXISTS(SELECT * FROM teacher WHERE login = i_login);

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION loginandpasscheck(character varying, character varying)
  OWNER TO postgres;
