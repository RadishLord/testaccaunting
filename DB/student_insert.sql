﻿CREATE OR REPLACE FUNCTION student_insert(p_id_group int, p_fio varchar, p_number_markbook varchar)
RETURNS void AS
$BODY$
BEGIN

	INSERT INTO student (id_group , fio, number_markbook) VALUES
	(p_id_group, p_fio, p_number_markbook);

END;
$BODY$
LANGUAGE plpgsql;

SELECT student_insert('1','Иванов BD', '20410031');