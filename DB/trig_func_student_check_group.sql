﻿--триггерная функция на вставку в student проверяет существование группы в study_group

CREATE OR REPLACE FUNCTION trig_func_student_check_group()
RETURNS trigger AS
$BODY$
BEGIN
	IF EXISTS(SELECT * FROM study_group WHERE id_group = NEW.id_group) THEN
		RAISE NOTICE 'Триггер trig_func_student_check_group - ОК ';
		return NEW;
	END IF;
	RAISE NOTICE 'Триггер trig_func_student_check_group - ERROR ';
	return OLD;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;


--тригер 
CREATE TRIGGER trigger_student_check_group
BEFORE INSERT OR UPDATE ON student
FOR EACH ROW
EXECUTE PROCEDURE trig_func_student_check_group();
