﻿--Удалении дочерних дистракторов при удалении вопроса
DROP  FUNCTION IF EXISTS delete_distractor_trigger_func() CASCADE;
CREATE FUNCTION delete_distractor_trigger_func () RETURNS trigger AS $$
DECLARE
p_id integer;
BEGIN

	p_id = old.id_quest;
	
	RAISE NOTICE 'Сработал триггер delete_distractor_trigger';
	
	IF (EXISTS (SELECT * FROM distractor WHERE id_quest = p_id))THEN
		RAISE NOTICE 'distr exist';
		DELETE FROM distractor WHERE id_quest = p_id;
	END IF;
        
        RETURN old;
END;
$$ LANGUAGE plpgsql;


create trigger delete_distractor_trigger
BEFORE  delete on question for row
execute procedure delete_distractor_trigger_func();

