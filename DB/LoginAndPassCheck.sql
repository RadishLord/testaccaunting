﻿CREATE OR REPLACE FUNCTION Login_and_pass_check(i_login varchar)
RETURNS table(o_login varchar, о_passw varchar, о_fio varchar) AS
$BODY$
BEGIN

	RETURN QUERY SELECT login,password,fio FROM teacher WHERE login = i_login;

END;
$BODY$
LANGUAGE plpgsql;
