﻿
CREATE OR REPLACE FUNCTION delete_distr(i_id integer)
  RETURNS void AS
$BODY$
BEGIN
	DELETE  FROM distractor
	WHERE id_distractor = i_id;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

