﻿CREATE OR REPLACE FUNCTION add_quest_and_return_id(i_id_test int, i_text character varying, i_number_quest int, i_cost real)
  RETURNS integer AS
$BODY$
DECLARE
o_id int;
BEGIN
	
	INSERT INTO question (id_test, text_quest, number_quest, cost_quest) VALUES
	(i_id_test, i_text, i_number_quest, i_cost);

	RETURN  id_quest FROM question WHERE id_test = i_id_test AND text_quest = i_text AND number_quest = i_number_quest AND cost_quest = i_cost;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;


SELECT add_quest_and_return_id('2','bla bla','12','2');