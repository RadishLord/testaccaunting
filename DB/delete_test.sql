﻿CREATE OR REPLACE FUNCTION delete_test(i_id integer)
  RETURNS void AS
$BODY$
BEGIN
	DELETE  FROM test
	WHERE id_test = i_id;
END;
$BODY$
  LANGUAGE plpgsql;