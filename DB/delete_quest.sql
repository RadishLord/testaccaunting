﻿CREATE OR REPLACE FUNCTION delete_quest(i_id integer)
  RETURNS void AS
$BODY$
BEGIN
	DELETE  FROM question
	WHERE id_quest = i_id;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
