﻿CREATE OR REPLACE FUNCTION group_update(i_id int, i_name varchar, i_cathedra varchar, i_rec_year date)
RETURNS void AS
$BODY$
BEGIN

	UPDATE study_group SET 
	name_group = i_name,
	cathedra = i_cathedra,
	recruitment_year = i_rec_year
 

	WHERE id_group = i_id;
	
END;
$BODY$
LANGUAGE plpgsql;

