﻿-- Function: get_teacher_data(character varying, character varying)

-- DROP FUNCTION get_teacher_data(character varying, character varying);

CREATE OR REPLACE FUNCTION get_teacher_data(IN i_login character varying, IN i_password character varying,
OUT o_fio varchar, 
OUT o_id integer)
  RETURNS SETOF record AS
$BODY$
BEGIN

	FOR o_fio, o_id IN SELECT fio, id_teacher FROM teacher WHERE login = i_login AND password = i_password LOOP
		RETURN next;
	END LOOP;
	
	RETURN;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION get_teacher_data(character varying, character varying)
  OWNER TO postgres;


SELECT * FROM get_teacher_data('daniel','qwerty');