﻿CREATE FUNCTION distr_count_decrement () RETURNS trigger AS $$
BEGIN

	UPDATE question   
	SET count_distractor = count_distractor - 1 WHERE id_quest = old.id_quest;

        
        RETURN old;
END;
$$ LANGUAGE plpgsql;

drop trigger if exists distr_count_decrement_trigger 
on distractor;
create trigger distr_count_decrement_trigger
before delete on distractor for row
execute procedure distr_count_decrement();