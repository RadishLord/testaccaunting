﻿CREATE OR REPLACE FUNCTION add_group(i_name character varying, i_cathedra character varying, i_year date)
  RETURNS void AS
$BODY$
BEGIN
	
	INSERT INTO study_group (name_group,cathedra ,recruitment_year) VALUES
	(i_name, i_cathedra, i_year);

END;
$BODY$
LANGUAGE plpgsql VOLATILE

