﻿CREATE OR REPLACE FUNCTION add_test(i_id_teacher int, i_name varchar, i_count_quest int)
  RETURNS void AS
$BODY$
BEGIN
	
	INSERT INTO test (id_teacher,name_test ,question_count) VALUES
	(i_id_teacher, i_name, i_count_quest);

END;
$BODY$
  LANGUAGE plpgsql VOLATILE;